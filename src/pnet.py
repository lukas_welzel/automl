import numpy as np
import torch
import torch.nn as nn
dev = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
from torch.nn.functional import log_softmax, nll_loss
from networks import Conv4
# from torchviz import make_dot

class PrototypicalNetwork(nn.Module):

    def __init__(self, num_ways, input_size, similarity="euclidean", **kwargs):
        # euclidean means that we use the negative squared Euclidean distance as kernel
        super().__init__()
        self.num_ways = num_ways
        self.input_size = input_size
        self.criterion = nn.CrossEntropyLoss()

        self.network = Conv4(self.num_ways, img_size=int(input_size**0.5))
        self.network = self.network.to(device=dev)
        self.similarity = similarity

        self.distance = self.euclidean_distance

        self.make_img = True
        self.iters = 0


    def apply(self, x_supp, y_supp, x_query, y_query, training=False):
        """
        Function that applies the Prototypical network to a given task and returns the predictions 
        on the query set as well as the loss on the query set

        :param x_supp (torch.Tensor): support set images
        :param y_supp (torch.Tensor): support set labels
        :param x_query (torch.Tensor): query set images
        :param y_query (torch.Tensor): query set labels
        :param training (bool): whether we are in training mode

        :return:
          - query_preds (torch.Tensor): our predictions for all query inputs of shape [#query inputs, #classes]
          - query_loss (torch.Tensor): the cross-entropy loss between the query predictions and y_query
        """
        # TODO: implement this function

        n_classes = len(torch.unique(y_supp))

        embedding_supp = self.network.get_infeatures(x_supp)
        embedding_query = self.network.get_infeatures(x_query)

        prototypes = torch.cat([embedding_supp[torch.nonzero(y_supp == label)].mean(0) for label in range(n_classes)])

        distance = torch.cdist(embedding_query, prototypes)

        query_loss = self.criterion(-distance, y_query)

        query_preds = -distance  # torch.softmax(-distance, dim=1)

        # if self.make_img:
        #     input_names = ['X']
        #     output_names = ['yhat']
        #     torch.onnx.export(self.network, x_query, 'pnets_torchviz.onnx', input_names=input_names, output_names=output_names)
        #     self.make_img = False

        # if self.iters % 100 == 0:
        #     display_sample(x_sorted)
        # self.iters += 1


        if training:
            query_loss.backward()  # do not remove this if statement, otherwise it won't train


        return query_preds, query_loss

    @staticmethod
    def euclidean_distance(x: torch.Tensor, y: torch.Tensor):
        dims = (x.size(dim=0), y.size(dim=0), x.size(dim=1))
        x = x.unsqueeze(1).expand(*dims)
        y = y.unsqueeze(0).expand(*dims)
        dist = torch.pow(x - y, 2).sum(2)
        return dist

    @staticmethod
    def prepare_samples(x_supp, y_supp, x_query, y_query, n_supp):
        x = torch.concat([x_supp,
                          x_query], dim=0).to(dev)
        y = torch.concat([y_supp,
                          y_query], dim=0).to(dev)

        idxs_sorted = torch.argsort(y)
        mapping = torch.argsort(idxs_sorted)
        idxs_support = mapping[:n_supp]
        idxs_query = mapping[n_supp:]

        x_sorted = x[idxs_sorted]
        y_sorted = y[idxs_sorted]

        return x, y, x_sorted, y_sorted, idxs_support, idxs_query

# from torchvision.utils import make_grid
# import matplotlib.pyplot as plt

# def display_sample(sample):
#     shape = sample.shape
#     shape = list(shape)
#     shape[1] = 3
#     sample.expand(shape)
#     img = make_grid(sample, nrow=20)
#     plt.figure(figsize=(12, 12))
#     plt.imshow(img.permute(1, 2, 0).cpu())
#     plt.show()
#     plt.close()