import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import h5py
from scipy.signal import savgol_filter

# labels = [
#     "Protonet (forward)",
#     "Protonet (get_infeatures)",
#     "1. ord. MAML",
#     "2. ord. MAML"
# ]

labels = [
    "Protonet (5 Way)",
    "Protonet (10 Way)",
    "Protonet (20 Way)",
    "1. ord. MAML (5 Way)",
    "1. ord. MAML (10 Way)",
    "1. ord. MAML (20 Way)",
]

colors = [
    "blue",
    "purple",
    "teal",
    "red",
    "orange",
    "peru",
]

colors2 = [
    "lightblue",
    "magenta",
    "cyan",
    "salmon",
    "yellow",
    "bisque"
]

ls = [
    "solid",
    "-.",
    "dashed",
    "dotted",
    ':',
    '--'
]

hatch = [
    "/",
    "x",
    "\\",
    ".",
    "o",
    "||"
]

def main_task1_plot():
    fig, axes = plt.subplots(1, 2, constrained_layout=True,
                             num="Task1",
                             figsize=(19, 6))

    axes = np.array(axes).flatten()

    yaxes = [
        r"Accuracy ($\hat{Acc}$)",
        r"Loss ($\hat{L}$)"
    ]

    # data_dirs = [
    #     "./results/pnet_default_settings_full.h5",
    #     "./results/pnet_infeatures_default.h5",
    #     "./results/1st_maml_default_full.h5",
    #     "./results/2nd_maml_default_full.h5"
    # ]

    data_dirs = [
        "./results/pnet_infeatures_default.h5",
        "./results/pnet_infeatures_default_num_ways10.h5",
        "./results/pnet_infeatures_default_num_ways20.h5",
        "./results/1st_maml_default_full.h5",
        "./results/1st_maml_num_ways10_full.h5",
        "./results/1st_maml_num_ways20_full.h5",

    ]

    for i in range(len(data_dirs)):
        data_dirs[i] = Path(data_dirs[i])

    for j, (ax, method) in enumerate(zip(axes, [plot_accuracy, plot_losses])):
        plt.sca(ax)
        ax.set_ylabel(yaxes[j])
        ax.set_xlabel("Episodes")

        for i, (label, data_dir) in enumerate(zip(labels, data_dirs)):
            header, data = read_h5(data_dir)

            # for key, value in data.items():
            #     print(key, value.shape)

            method(data, label,
                   color=colors[i], ls=ls[i], hatch=hatch[i], color2=colors2[i])

    plt.savefig("accuracy-loss-num_ways.png", dpi=350)
    plt.show()





def read_h5(loc="NULL.h5"):
    """
    Gets the data from a specific experiment, returns all values in a dict
    Each row in the dict values is an experiment i.e. history.shape = (3, 150)
                                                        -> 3 experiments, 150 function evaluations
    The header has the corresponding experiment setup data in arrays that correspond to the rows of e.g. history
    """
    data = {}
    with h5py.File(loc, "r") as file:

        for key, val in file.items():
            data[key] = np.array(file[key])

        header = dict(file.attrs)

    return header, data

def read_result(dir):
    pass

def plot_losses(data, label, color=None, color2=None, hatch=None, ls=None, plot_smooth=True, plot_full=False,
                window=51, use_log=True):
    fig = plt.gcf()
    ax = fig.gca()

    # print(data)

    val_losses = data["val_losses"]
    test_losses = data["losses"]

    print(val_losses.shape)
    print(test_losses.shape)

    mean_val_losses = np.nanmean(val_losses, axis=(0, 2))
    # mean_val_losses *= 1 / np.mean(mean_val_losses[:5])
    mean_test_losses = np.nanmean(test_losses, axis=0)
   #  mean_test_losses *= 1 / np.mean(mean_test_losses[5])

    std_val_losses = np.nanstd(val_losses, axis=(0, 2)) #/ np.mean(mean_val_losses[:5])
    std_test_losses = np.nanstd(test_losses, axis=0)# / np.mean(mean_test_losses[:5])

    x_val = 40000/80 * np.arange(len(mean_val_losses))
    x_test = np.arange(len(mean_test_losses))

    scale = 0.1

    if plot_full:
        ax.plot(x_val, mean_val_losses,
                label=label+r" $\hat{L}_{val}$",
                color=color,
                ls=ls)

        ax.fill_between(x_val,
                        mean_val_losses + scale * std_val_losses,
                        mean_val_losses - scale * std_val_losses,
                        #label=label+r" $L_{val}$, 1$\sigma$CI",
                        color=color,
                        hatch=hatch,
                        alpha=0.3)

        ax.plot(x_test, mean_test_losses,
                label=label + r" $\hat{L}_{train}$",
                color=color2,
                ls=ls)

        ax.fill_between(x_test,
                        mean_test_losses + scale * std_test_losses,
                        mean_test_losses - scale * std_test_losses,
                        #label=label + r" $L_{train}$, 1$\sigma$CI",
                        color=color2,
                        hatch=hatch,
                        alpha=0.3)

    if plot_smooth:
        smooth_mean_val_losses = np.clip(savgol_filter(mean_val_losses, window, 1), a_min=0., a_max=None)
        # smooth_mean_val_losses *= 1 / np.mean(smooth_mean_val_losses[:5])
        smooth_mean_test_losses = np.clip(savgol_filter(mean_test_losses, window * 10 + 1, 1), a_min=0., a_max=None)
        # smooth_mean_test_losses *= 1 / np.mean(smooth_mean_test_losses[:5])

        smooth_std_val_losses = savgol_filter(std_val_losses, window, 1) # / np.mean(smooth_mean_val_losses[:5])
        smooth_std_test_losses = savgol_filter(std_test_losses, window * 10 + 1, 1) # / np.mean(smooth_mean_test_losses[:5])

        ax.plot(x_val, smooth_mean_val_losses,
                label=label + r" $\hat{L}_{val}$",
                color=color,
                ls=ls)

        ax.fill_between(x_val,
                        np.clip(smooth_mean_val_losses + scale * smooth_std_val_losses, a_min=0., a_max=None),
                        np.clip(smooth_mean_val_losses - scale * smooth_std_val_losses, a_min=0., a_max=None),
                        #label=label + r" $L_{val}$, 1$\sigma$CI",
                        color=color,
                        hatch=hatch,
                        alpha=0.3)

        ax.plot(x_test, smooth_mean_test_losses,
                label=label + r" $\hat{L}_{train}$",
                color=color2,
                ls=ls)

        ax.fill_between(x_test,
                        np.clip(smooth_mean_test_losses + scale * smooth_std_test_losses, a_min=0, a_max=None),
                        np.clip(smooth_mean_test_losses - scale * smooth_std_test_losses, a_min=0, a_max=None),
                        #label=label + r" $L_{train}$, 1$\sigma$CI",
                        color=color2,
                        hatch=hatch,
                        alpha=0.3)



        ax.legend()


        if use_log:
            ax.set_yscale("log")
            ax.set_ylim(0.001, 2.0)
        else:
            ax.set_ylim(0., 1.5)

    return

def plot_accuracy(data, label, color=None, color2=None, hatch=None, ls=None, plot_smooth=True, plot_full=False,
                  window=51, **kwargs):
    ax = plt.gca()

    full_alpha = 1.
    if plot_smooth:
        full_alpha = 0.1

    hatch_alpha = 0.1

    accuracies = data["accuracies"]
    # accuracies = 1. - accuracies
    val_accuracies = data["val_accuracies"]
    # val_accuracies = 1. - val_accuracies

    print(accuracies.shape)
    print(val_accuracies.shape)

    mean_accuracies = np.mean(accuracies, axis=0)
    std_accuracies = np.std(accuracies, axis=0)

    mean_val_accuracies = np.nanmean(val_accuracies, axis=(0, 2))
    std_val_accuracies = np.nanstd(val_accuracies, axis=(0, 2))


    x = np.arange(len(mean_accuracies))
    x_val = 40000/80 * np.arange(len(mean_val_accuracies))

    scale = 1.

    if plot_full:
        ax.plot(x, mean_accuracies,
                label=label+r" $\hat{Acc}$",
                color=color,
                ls=ls,
                alpha=full_alpha + 0.1)

        ax.fill_between(x,
                        mean_accuracies + scale * std_accuracies,
                        mean_accuracies - scale * std_accuracies,
                        #label=label+r" $\hat{Acc}$, 1$\sigma$CI",
                        color=color,
                        hatch=hatch,
                        alpha=full_alpha)

        ax.plot(x_val, mean_val_accuracies,
                label=label+r" $\hat{Acc}$",
                color=color2,
                ls=ls,
                alpha=full_alpha + 0.1)

        ax.fill_between(x_val,
                        mean_val_accuracies + scale * std_val_accuracies,
                        mean_val_accuracies - scale * std_val_accuracies,
                        #label=label+r" $\hat{Acc}$, 1$\sigma$CI",
                        color=color2,
                        hatch=hatch,
                        alpha=full_alpha)

    if plot_smooth:
        smooth_mean_acc = np.clip(savgol_filter(mean_accuracies, window * 10 + 1, 1), a_min=0, a_max=1.)
        smooth_std_acc = savgol_filter(std_accuracies, window * 10 + 1, 1)
        ax.plot(x, smooth_mean_acc,
                label=label + r" $\hat{Acc}$",
                color=color,
                ls=ls,
                alpha=1.)

        ax.fill_between(x,
                        np.clip(smooth_mean_acc + scale * smooth_std_acc, a_min=0, a_max=1.),
                        np.clip(smooth_mean_acc - scale * smooth_std_acc, a_min=0, a_max=1.),
                        #label=label + r" $\hat{Acc}$, 1$\sigma$CI ",
                        color=color,
                        hatch=hatch,
                        alpha=hatch_alpha)

        smooth_mean_val_acc = np.clip(savgol_filter(mean_val_accuracies, window, 1), a_min=0, a_max=1.)
        smooth_std_val_acc = savgol_filter(std_val_accuracies, window, 1)
        ax.plot(x_val, smooth_mean_val_acc,
                label=label + r" $\hat{Acc}_{val}$",
                color=color2,
                ls=ls,
                alpha=1.)

        ax.fill_between(x_val,
                        np.clip(smooth_mean_val_acc + scale * smooth_std_val_acc, a_min=0, a_max=1.),
                        np.clip(smooth_mean_val_acc - scale * smooth_std_val_acc, a_min=0, a_max=1.),
                        #label=label + r" $\hat{Acc}$, 1$\sigma$CI ",
                        color=color2,
                        hatch=hatch,
                        alpha=hatch_alpha)

        ax.annotate("For both figures: \nN=5\n"+r"Shaded areas are $1\sigma$ confidence intervals."+"\nAll curves are smoothed.",
                    xy=(0.01, 0.01),
                    xycoords='axes fraction',
                    ha="left", va="bottom")

        # def forward(x):
        #     return x**2 #np.exp(x)
        #
        # def inverse(x):
        #     return x**(1/2) # np.log(x)

        ax.legend(loc="lower right")
        # ax.set_yscale('function', functions=(forward, inverse))
        #ax.set_ylim(0.000000001, 1.)
        ax.set_ylim(0.4, None)
        ax.set_xlim(None, None)

    return



if __name__ == '__main__':
    main_task1_plot()