#########################################################################################
# The assignment file. While it is allowed to change this entire file, we highly
# recommend using the provided template. YOU MUST USE THE RANGES AND HYPERPARAMETERS SPECIFIED
# IN GET_RANGES AND GET_CONFIG_PERFORMAMCE (IN SHORT: USE OUR SURROGATE PROBLEMS)
#########################################################################################

import numpy as np
import argparse
import os
import pandas as pd
import matplotlib.pyplot as plt
from utils import normal_dist

from utils import GET_CONFIG_PERFORMANCE, GET_RANGES, SampleType, \
    ParamType  # make sure to make use of ParamType and SampleType in your code

parser = argparse.ArgumentParser()
parser.add_argument('--problem', choices=['good_range', 'bad_range', 'interactive'],
                    default='good_range')  # required=False)
parser.add_argument('--algorithm', choices=['rs', 'tpe'], default="tpe")  # required=False)
parser.add_argument('--gamma', type=float, default=0.1)
parser.add_argument('--random_warmup', type=int, default=10)
parser.add_argument('--seed', type=int, default=42)


rng = np.random.default_rng()
global_verbose = False

# since we already import numpy this should be ok right? Only use is explained below.
from numpy import warnings
# g and l can be empty arrays which is expected behaviour in our code,
# the issues are solved through adjusting EI which is more stable and allows finer control.
# numpy doesnt like this so we catch the warnings. This is not thread safe. Cool.
# It is also probably bad style but it is efficient.
# comes from g and l, expected
warnings.filterwarnings('ignore', message="Mean of empty slice", category=RuntimeWarning)
# comes from normalizing EI = l/g with singularities, caught internally using try/except for speeeeed
warnings.filterwarnings('ignore', message="invalid value encountered in true_divide", category=RuntimeWarning)
# comes from overflows that are caused by very small non-zero probabilities for g, solved internally
warnings.filterwarnings('ignore', message="overflow encountered in true_divide", category=RuntimeWarning)
warnings.filterwarnings('ignore', message="overflow encountered in multiply", category=RuntimeWarning)


class Sampler():
    subclasses = {}
    @classmethod
    def register_subclass(cls, sample_type):

        if global_verbose:
            print(f"Register Subclass {sample_type}")

        def decorator(subclass):
            cls.subclasses[sample_type] = subclass
            return subclass

        return decorator

    @classmethod
    def create(cls, sample_type, **kwargs):
        if sample_type not in cls.subclasses:
            raise ValueError(f'Bad type {sample_type}')

        return cls.subclasses[sample_type](sample_type, **kwargs)

    def __init__(self, sample_type, interval, wanted_dtype):
        super(Sampler, self).__init__()

        self.sample_type = sample_type
        self.wanted_dtype = wanted_dtype

        self.interval = interval
        self.x = np.arange(1)

    def get_space(self):
        return self.x

    def sample(self, x, pdf, n, **kwargs):
        return self._sample_discrete(x, pdf, n, **kwargs)

    @staticmethod
    def _sample_discrete(x, pdf, n, **kwargs):

        samples = rng.choice(x, size=n, replace=True, p=pdf)
        return samples

    @staticmethod
    def _sample_uniform(x, pdf, n, **kwargs):
        # TODO: non uniform spacing
        cumpdf = np.cumsum(pdf)
        cumpdf *= 1 / cumpdf[-1]

        samples = rng.uniform(size=n)

        ind1 = np.searchsorted(cumpdf, samples)

        ind0 = np.where(ind1 == 0, 0, ind1 - 1)
        ind1[ind0 == 0] = 1

        frac1 = (samples - cumpdf[ind0]) / (cumpdf[ind1] - cumpdf[ind0])
        samples = x[ind0] * (1 - frac1) + x[ind1] * frac1
        return samples

    def _sample_log_uniform(self, x, pdf, n, base=10., **kwargs):
        # Log-Uniform (reciprocal) distributions are continous
        # applying them to discrete sampling (e.g. integers) is a bit funky, however the conversions below work fine
        # issues might be that the type conversions have some rounding errors, but it is not worth fixing this atm

        samples = np.power(base, self._sample_uniform(x=x, pdf=pdf, n=n))

        return samples.astype(self.wanted_dtype)


@Sampler.register_subclass('Discrete')
class DiscreteSampler(Sampler):
    def __init__(self, sample_type, interval, wanted_dtype,
                 **kwargs):
        super(DiscreteSampler, self).__init__(sample_type, interval, wanted_dtype)

        self.interval = interval

        try:
            # this is horrible to use for integers because it creates large np.aranges for large int search spaces
            self.x = np.arange(np.min(self.interval), np.max(self.interval) + 1)
        except TypeError:
            self.x = np.arange(len(self.interval))

    def sample(self, x, pdf, n, **kwargs):
        samples = self._sample_discrete(x, pdf, n, **kwargs)
        return samples


@Sampler.register_subclass('Uniform')
class UniformSampler(Sampler):
    def __init__(self, sample_type, interval, distr_resolution, wanted_dtype=float,
                 **kwargs):
        super(UniformSampler, self).__init__(sample_type, interval, wanted_dtype)

        self.x = np.linspace(np.min(self.interval),
                             np.max(self.interval),
                             distr_resolution)

    def sample(self, x, pdf, n, **kwargs):
        return self._sample_uniform(x, pdf, n, wanted_dtype=float, **kwargs)


@Sampler.register_subclass('LogUniform')
class LogUniformSampler(Sampler):
    def __init__(self, sample_type, interval, distr_resolution, wanted_dtype,
                 **kwargs):
        super(LogUniformSampler, self).__init__(sample_type, interval, wanted_dtype)

        self.distr_resolution = distr_resolution

        if wanted_dtype == int:
            self.x = np.unique(np.linspace(np.min(self.interval),
                                           np.max(self.interval),
                                           self.distr_resolution).astype(int))

            self.distr_resolution = len(self.x)

        else:
            self.x = np.geomspace(np.min(self.interval),
                                  np.max(self.interval),
                                  self.distr_resolution)

        self._x = np.linspace(np.log10(np.min(self.interval), dtype=float),
                              np.log10(np.max(self.interval), dtype=float),
                              self.distr_resolution)

    def sample(self, x, pdf, n, **kwargs):
        return self._sample_log_uniform(self._x, pdf, n, base=10., **kwargs)


class HyperPara(ParamType, SampleType):

    PARAMTYPE_TO_CLASS_MAP = [
        "Categorical",
        "Real",
        "Integer"
    ]

    SAMPLETYPE_TO_CLASS_MAP = [
        "Uniform",
        "LogUniform",
        "Discrete"
    ]

    subclasses = {}
    @classmethod
    def register_subclass(cls, paramtype):
        if global_verbose:
            print(f"Register Subclass {paramtype}")

        def decorator(subclass):
            cls.subclasses[paramtype] = subclass
            return subclass

        return decorator

    @classmethod
    def create(cls, paramtype, **kwargs):

        paramtype = cls.PARAMTYPE_TO_CLASS_MAP[paramtype]

        if paramtype not in cls.subclasses:
            raise ValueError(f'Bad type {paramtype}')

        return cls.subclasses[paramtype](paramtype, **kwargs)

    def __init__(self, para_type, sample_type, name="default_HP", interval=None,
                 distr_resolution=1000, function_evaluations=10, burn_in=30,
                 gamma=0.1, condition=None, **kwargs):
        super(HyperPara, self).__init__()

        self.name = name

        self.__PARATYPE = para_type
        self.__SAMPLETYPE = sample_type

        self.para_type = para_type

        if para_type == "Categorical":
            self.sample_type = "Discrete"
        else:
            self.sample_type = self.__class__.SAMPLETYPE_TO_CLASS_MAP[sample_type]

        self.burn_in = burn_in

        self.interval = interval
        self.sampler = Sampler.create(self.sample_type, interval=self.interval,
                                      distr_resolution=distr_resolution, wanted_dtype=None)
        self.x = self.sampler.get_space()

        if condition is None:
            self.condition = self.eval_condition
        else:
            self.condition = condition

        # TODO implement these as masked arrays using compliance as a mask
        self.initial_guess = np.ones_like(self.x) / len(self.x)

        self._distributions = np.full(shape=(function_evaluations+1, len(self.x)),
                                     fill_value=np.nan, dtype=float)

        self.sample_history = np.full(function_evaluations + 1, fill_value=np.nan)
        self.compliance = np.full_like(self.sample_history, fill_value=False).astype(bool)
        self._n_samples = 0
        self._n_losses = 0
        self.losses = np.full(function_evaluations+1, fill_value=np.nan)
        self.gamma = gamma

        # initialize
        self._distributions[-1] = self.initial_guess
        self.sample_history[-1] = np.nan
        self.compliance[-1] = True
        self.losses[-1] = np.inf
        self.up2date = True

        self._sample_from = None
        self._rescale_sample = None
        self.__wanted_dtype = None


    def __str__(self):
        s = f"===============================\n" \
            f"Name:     {self.name}\n" \
            f"Type:     {self.para_type}\n" \
            f"Sample:   {self.sample_type}\n" \
            f"From:     {self.interval}"
        return s

    def eval_condition(self, x):
        return True

    def sample(self, n, **kwargs):
        # if I pass self.distr_expected_improvement I am not sure if it is static i.e. if it is the same for all samples
        # expected behaviour is static since the rngs should make a static copy, but we might want to enforce this
        # due to referencing in python this would need the deepcopy module which we dont have
        samples = self.sampler.sample(self.x, self.distr_expected_improvement, n, **kwargs)
        self.sample_history[self._n_samples:self._n_samples + n] = samples
        self._n_samples += n
        samples = self.translate_samples(samples)
        self.up2date = False
        return samples

    def translate_samples(self, samples, **kwargs):
        return samples

    def _get_single_distribution(self, i, sample, loss, **kwargs):
        distribution = np.zeros_like(self.x)
        distribution[sample] = 1.
        return distribution

    def set_loss(self, loss):
        _loss = np.array(loss).flatten()
        self.losses[self._n_losses:self._n_losses + len(_loss)] = loss
        self._n_losses += len(_loss)


    @property
    def sigmas(self):
        return np.zeros_like(self.sample_history)

    @staticmethod
    def normalize(pmf):
        cmf = np.cumsum(pmf)
        pdf = pmf / cmf[-1]
        return pdf

    @property
    def distributions(self):
        if not self.up2date:
            sigmas = self.sigmas

            for i in np.arange(0, len(self.sample_history[:-1]), dtype=int)[self.valid[:-1]]:
                sample = self.sample_history[i]
                loss = self.losses[i]
                sigma = sigmas[i]
                self._distributions[i] = self.normalize(self._get_single_distribution(i,
                                                                                      sample=sample, loss=loss,
                                                                                      sigma=sigma))
            self.up2date = True

        return self._distributions

    @property
    def distr_expected_improvement(self):
        if self.valid[-1]:
            self.valid[-1] = np.logical_and(self.n_valid_samples > self.burn_in,
                                            self.need_init)

        # EI can have singularities if bad=0 at any, in that case we reintroduce the initial distribution for stability
        # the influence of the initial distribution is asymptotic as 1/n
        with np.errstate(divide='raise'):
            try:
                EI = (self.distr_good / self.distr_bad)
                EI[np.isnan(EI)] = 0.
                expected_improvement = (self.n_valid_samples * EI
                                        + (self.distributions[-1] * self.valid[-1])) \
                                       / (self.n_valid_samples + (1 * self.valid[-1]))
            except FloatingPointError:
                non_singular = ~(self.distr_bad == 0)
                EI = np.zeros_like(self.distr_good)
                EI[non_singular] = (self.distr_good[non_singular] / self.distr_bad[non_singular])
                EI[np.isnan(EI)] = 0.
                expected_improvement = (self.n_valid_samples * EI
                                        + self.distributions[-1]) / (self.n_valid_samples + 1)

        return self.normalize(expected_improvement).astype(float)

    @property
    def distr_good(self):
        return self.normalize(np.nanmean(self.distributions[self.where_good & self.valid], axis=0)).astype(float)

    @property
    def distr_bad(self):
        return self.normalize(np.nanmean(self.distributions[self.where_bad & self.valid], axis=0)).astype(float)

    @property
    def n_valid_samples(self):
        return len(self.sample_history[:-1][self.valid[:-1]])

    @property
    def idx_sorted_loss(self):
        return np.argsort(self.losses)

    @property
    def idx_sorted_x(self):
        return np.argsort(self.sample_history)

    @property
    def idx_good(self):
        # use argpartition trick
        n_smallest = int(np.ceil(self.n_valid_samples * self.gamma))
        idx_sorted_valid = self.valid[self.idx_sorted_loss]
        return self.idx_sorted_loss[idx_sorted_valid][:n_smallest]

    @property
    def where_good(self):
        idx_good = self.idx_good
        good = np.full_like(self.sample_history, fill_value=False)
        good[idx_good] = True
        return good.astype(bool)

    @property
    def where_bad(self):
        bad = ~self.where_good
        bad[-1] = False
        return bad.astype(bool)

    @property
    def valid(self):
        # valid = self.compliance
        # TODO: check if sample already deposited using _n_samples?
        # should not be necessary since compliance always is False unless explicitly set to Ture
        return self.compliance.astype(bool)

    @property
    def need_init(self):
        col_sum = np.nansum(self.distributions[:-1], axis=1)
        return np.any(col_sum == 0)

    def write_axis(self, **kwargs):
        ax = plt.gca()

        left, width = .25, .5
        bottom, height = .25, .5
        right = left + width
        top = bottom + height

        ax.text(0.5 * (left + right), 0.5 * (bottom + top), 'NOT A REAL HP',
                horizontalalignment='center',
                verticalalignment='center',
                transform=ax.transAxes)


    def show_history(self, fig, ax, a, b, c, losses=None, color="black"):
        # fig, ax = plt.subplots(1, 1, constrained_layout=True)

        ax = plt.gca()

        self.compliance[-1] = False

        if losses is None:
            losses = self.losses
        else:
            losses = losses

        _loss = losses

        y_star = np.mean([np.max(_loss[self.where_good & self.valid]),
                          np.min(_loss[self.where_bad & self.valid])])
        ax.axhline(y_star, c="gray", ls=(0, (5, 10)), label=r"$y^{\star}$")

        # TODO: add overall sample density for n-> inf, sample density -> EI

        ax.scatter(self.sample_history[self.where_good & self.valid], _loss[self.where_good & self.valid],
                   marker="D", s=8, color="darkblue", alpha=0.3, zorder=0)
        ax.scatter(self.sample_history[self.where_bad & self.valid], _loss[self.where_bad & self.valid],
                   marker=".", s=8, color="darkred", alpha=0.3, zorder=0)

        # ax.set_title(f"{self.name} Survey")
        ax.set_xlabel(f"{self.name}")

        if self.sample_type == "LogUniform":
            ax.set_xscale("log")

        self.write_axis(_loss=_loss)
        ax_dist = plt.gca()

        if a == 0:
            ax.set_ylabel(r"Loss [-]")

        if a == 1 or c == 1:
            ax_dist.set_ylabel("Distribution [-]")

        # ax_dist.set_yscale("log")
        # ax_dist.set_ylim(1e-10, 1.)


        # ax.legend()
        # ax_dist.legend()
        # plt.show()


        return ax


@HyperPara.register_subclass('Categorical')
class CategoricalHP(HyperPara):
    def __init__(self, para_type, **kwargs):
        super(CategoricalHP, self).__init__(para_type, **kwargs)

        # update in case the categorical looks and smells like an integer but actually is not
        self.x = np.arange(len(self.x))
        self.sampler.x = self.x
        self.initial_guess = np.ones_like(self.x) / len(self.x)


    def translate_samples(self, samples, **kwargs):
        return np.array([self.interval[i] for i in samples])


    def _get_single_distribution(self, i, sample, **kwargs):
        distribution = np.zeros_like(self.x)
        distribution[sample.astype(int)] = 1.
        return distribution

    def write_axis(self, _loss):
        ax = plt.gca()

        width = 0.225

        unique_samples = np.array([np.where(self.sample_history[self.valid] == v)[0]
                                   for v in np.unique(self.sample_history[self.valid])],
                                  dtype=object)
        data = np.array([_loss[i] for i in unique_samples],
                        dtype=object)

        # TODO: This is funky. Sometimes data and positions are not compatible.
        #  I cant reliably replicate this bug even when I pass the same seed for everything seed.
        #  Rerunning fixes it. No idea why or how.

        ax.boxplot(data, positions=self.x, notch=True, showfliers=False, widths=width * 0.8,
                   zorder=99)

        ax_dist = ax.twinx()

        ax_dist.bar(self.x - 2 * width * 1.05, self.distr_good, width=width,
                    label=r"Good [$l(x)$] ", alpha=0.9, hatch="/", color="lightblue")
        ax_dist.bar(self.x - width * 1.05, self.distr_bad, width=width,
                    label=r"Bad   [$g(x)$] ", alpha=0.9, hatch="\\", color="crimson")
        ax_dist.bar(self.x + width * 1.05, self.distr_expected_improvement, width=width,
                    label=r"EI      [$l(x)/g(x)$] ", alpha=0.9, hatch="o", color="darkgreen")

        ax.set_xticks(self.x, self.interval)


@HyperPara.register_subclass('Integer')
class IntegerHP(HyperPara):
    def __init__(self, para_type, **kwargs):
        super(IntegerHP, self).__init__(para_type, **kwargs)

        self.wanted_dtype = int
        self.sampler.wanted_dtype = self.wanted_dtype

    @property
    def sigmas(self):
        idx_sorted_x = self.idx_sorted_x

        diff_right = np.zeros_like(self.sample_history)
        diff_right[1:] = np.abs(self.sample_history[idx_sorted_x][:-1] - self.sample_history[idx_sorted_x][1:])

        diff_left = np.zeros_like(self.sample_history[idx_sorted_x])
        diff_left[:-1] = np.abs(self.sample_history[idx_sorted_x][1:] - self.sample_history[idx_sorted_x][:-1])

        sigmas = np.maximum(diff_right, diff_left)

        # There can be samples that are on top of each other, i.e. their distance is zero.
        # We find these by finding zero distances in above array
        # This doesnt really happen (often) but better to be safe than sorry
        # It happened and caused an issue counter: 1    # increase if it causes issues again

        zero_dist_ind = np.argwhere(sigmas == 0)
        get_neighbours = lambda x: np.array([x - 1, x + 1])  # only for flat arrays
        for z in zero_dist_ind:
            neighbours = get_neighbours(z)
            try:
                while sigmas[neighbours[1]] == 0:  # what a shit show,
                    # why am I even checking for multiple zero distance samples...
                    # I guess it makes sense for non-categorical integers
                    neighbours[1] += 1
                sigmas[z] = np.max(sigmas[neighbours])
            except IndexError:
                try:
                    if neighbours[0] < 0:
                        left = self.interval[0]
                    else:
                        left = sigmas[neighbours[0]]
                except IndexError:
                    left = self.interval[0]
                try:
                    right = sigmas[neighbours[1]]
                except IndexError:
                    right = self.interval[1]

                sigmas[z] = np.max(left, right)

        # sigmas are now still sorted by x however we need the chronological order, below reorders them properly
        idx_return_order = np.argsort(idx_sorted_x)
        sigmas = sigmas[idx_return_order]

        return sigmas

    def _get_single_distribution(self, i, sample, loss, sigma=0, **kwargs):
        distribution = normal_dist(self.x,
                                   mean=sample, sd=sigma)
        return distribution

    def write_axis(self, **kwargs):
        ax = plt.gca()

        ax_dist = ax.twinx()

        ax_dist.plot(self.x, self.distr_good,
                     label=r"Good [$l(x)$]", ls="dashed", lw=2., color="lightblue")
        ax_dist.plot(self.x, self.distr_bad,
                     label=r"Bad   [$g(x)$]", ls="dotted", lw=2., color="crimson")
        ax_dist.plot(self.x, self.distr_expected_improvement,
                     label=r"EI      [$l(x) / g(x)$]", ls="solid", lw=2., color="darkgreen")


@HyperPara.register_subclass('Real')
class RealHP(IntegerHP):  # cool subclass registration of a sub-subclass
    def __init__(self, para_type, **kwargs):
        super(RealHP, self).__init__(para_type, **kwargs)

        self.wanted_dtype = float
        self.sampler.wanted_dtype = self.wanted_dtype

class TestCase(object):
    def __init__(self, hyperparameters):
        super(TestCase, self).__init__()

        self.hps = np.array(hyperparameters, dtype=object)

        self._names = np.array([hp.name for hp in self.hps])

    def __str__(self):
        s = "\n".join([hp.__str__() for hp in self.hps])
        s = "/////////// CONFIG  ///////////\n" + s + "\n///////////////////////////////"
        return s

    def sample(self, n=1, update=False, update_each=False):
        # so this is a bit dangerous; the array casts to float, even if some options are int.
        # as long as internal converters can handle the translation we are fine ;)


        arr = np.full(shape=(self.hps.size, n), fill_value=np.nan, dtype=object)
        for i, hp in enumerate(self.hps):
            arr[i] = hp.sample(n=n)  # sample updates internally

        arr = np.array([dict(zip(self._names, arr[:, j])) for j in np.arange(arr[0].size)])

        # while not needed I enforce condition compliance for a config.
        # here it is ok since the eval function in utils accepts the input,
        # however for real functions that would typically not be the case
        self.check_conditions(arr)
        arr = self.enforce_compliance(arr)

        if len(arr) == 1:
            return arr[0]

        return arr

    def check_conditions(self, arr):
        # the implementation in utils is truely unholy, makes vectorization impossible

        # even though in the examples there is only one condition
        # this would be able to conservatively handle compliance with multiple requirements
        for hp in self.hps:
            compliance = np.array([np.any(hp.condition(el)) for el in arr])
            hp.compliance[hp._n_samples-len(compliance):hp._n_samples] = compliance

    def enforce_compliance(self, arr):
        for i, config in enumerate(arr):
            for hp in self.hps:
                if ~hp.compliance[hp._n_samples + i - len(arr)]:
                    del arr[i][hp.name]

        return arr

    def set_losses(self, loss):
        for hp in self.hps:
            hp.set_loss(loss)


def random_search(problem, function_evaluations=150, verbose=True, **kwargs):
    """
    Function that performs random search on the given problem. It uses the
    ranges and sampling types defined in GET_RANGES(problem) (see utils.py).

    Arguments:
      - problem (str): the prolbem identifier
      - function_evaluations (int): the number of configurations to evaluate
      - **kwargs: any other keyword arguments

    Returns:
      - history (list): A list of the observed losses
      - configs (list): A list of the tried configurations. Every configuration is a dictionary
                        mapping hyperparameter names to the chosen values
    """
    method_name = "Random Search"

    history = np.full(shape=function_evaluations, fill_value=np.nan)
    # configs = np.full(shape=n, fill_value={})

    # get all information about the hyperparameters we want to tune for this problem
    # (see utils.py) for the form of this.
    RANGES = GET_RANGES(problem)

    hyperparameters = np.array([HyperPara.create(paramtype=values["type"],
                                                 name=key,
                                                 sample_type=values["sample"],
                                                 interval=values["range"],
                                                 function_evaluations=function_evaluations,
                                                 condition=values.get("condition", None),
                                                 **kwargs)
                                for (key, values) in RANGES.items()
                                ], dtype=object)
    case = TestCase(hyperparameters)

    if verbose:
        print(case)

    # all HP are sampled from the initial distribution without updating l, g, EI:
    # -> random search according to initial distribution.
    # Note: initial uniform distributions are sampled LogUniformly if the HP uses LogUniform sampling.
    #       supplying a LogUniform initial distribution in that case leads to LogLogUniform sampling.

    configs = case.sample(n=function_evaluations)

    for i in range(function_evaluations):
        sc = configs[i]
        loss = GET_CONFIG_PERFORMANCE(sc, problem=problem)
        history[i] = loss
        case.set_losses(loss=loss)

    if verbose:
        (a, b) = closest_rectangle(len(hyperparameters) + 1)

        fig, axes = plt.subplots(b, a, constrained_layout=True, sharey=True,
                                 figsize=(4 * b, 3 * a))
        fig.suptitle(f"Hyperparameter survey using {method_name} on {problem} problem.", fontsize="x-large", weight="bold")

        aa, bb = np.meshgrid(np.arange(a), np.arange(b))

        a_locs = np.array(
            [[_a / aa.max(), _b / bb.max(), _c / (len(hyperparameters) - 1)] for (_a, _b, _c) in zip(aa.flatten(),
                                                                                                     bb.flatten(),
                                                                                                     np.arange(
                                                                                                         len(hyperparameters)))])

        axes = np.array(axes).flatten()

        hh, ll = [], []

        for hp, ax, a_loc in zip(hyperparameters, axes, a_locs):
            plt.sca(ax)
            hp.show_history(fig, ax, *a_loc)
            handles, labels = plt.gca().get_legend_handles_labels()
            hh.append(handles), ll.append(labels)

        hh, ll = np.array(hh, dtype=object).flatten(), np.array(ll, dtype=object).flatten()

        legend_axis = True
        for ax in axes:
            if not ax.collections:
                if legend_axis:
                    bbox = ax.get_position()
                    legend_axis = False
                ax.axis("off")

        # massive mess to get nice labeling for all axes
        by_label = dict(zip(hh, ll))
        clean_by_label = {}
        for key, value in by_label.items():
            if value not in clean_by_label.values() and key not in clean_by_label.keys():
                clean_by_label[key] = value
        fig.legend(clean_by_label.keys(), clean_by_label.values(), bbox_to_anchor=bbox,
                   loc="center", fontsize="large", ncol=2,
                   title="Legend", title_fontproperties={"weight": "bold",
                                                         "size": "x-large"})

        plt.savefig(f"./results/RS-{problem}.png", dpi=400)

        plt.show()
        plt.clf()
    else:
        plt.clf()

    return history, configs


def tpe(problem, function_evaluations=150, random_warmup=10, gamma=0.2, verbose=True, **kwargs):
    """
    Function that uses Tree Parzen Estimator (TPE) to tune the hyperparameters of the
    given problem. It uses the ranges and sampling types defined in GET_RANGES(problem)
    (see utils.py).

    Arguments:
      - problem (str): the prolbem identifier
      - function_evaluations (int): the number of configurations to evaluate
      - random_warmup (int): the number of initial iterations during which we perform random
                             search
      - gamma: the value of gamma that determines the cutting point [good partition, bad partition]
      - **kwargs: any other keyword arguments

    Returns:
      - history (list): A list of the observed losses
      - configs (list): A list of the tried configurations. Every configuration is a dictionary
                        mapping hyperparameter names to the chosen values
    """

    method_name = "TPE"

    history = np.full(shape=function_evaluations, fill_value=np.nan)
    # configs = np.full(shape=n, fill_value={})

    # get all information about the hyperparameters we want to tune for this problem
    # (see utils.py) for the form of this.
    RANGES = GET_RANGES(problem)

    hyperparameters = np.array([HyperPara.create(paramtype=values["type"],
                                                 name=key,
                                          sample_type=values["sample"],
                                          interval=values["range"],
                                          function_evaluations=function_evaluations,
                                          gamma=gamma,
                                          condition=values.get("condition", None),
                                          **kwargs)
                                for (key, values) in RANGES.items()
                                ], dtype=object)
    case = TestCase(hyperparameters)

    if verbose:
        print(case)

    configs = np.full(function_evaluations, fill_value={})

    warmup_configs = case.sample(n=random_warmup)
    configs[:random_warmup] = warmup_configs

    for i in range(random_warmup):
        sc = configs[i]
        loss = GET_CONFIG_PERFORMANCE(sc, problem=problem)
        history[i] = loss
        case.set_losses(loss=loss)

    for i in range(random_warmup, function_evaluations):
        sc = case.sample(n=1)
        configs[i] = sc
        loss = GET_CONFIG_PERFORMANCE(sc, problem=problem)
        history[i] = loss
        case.set_losses(loss=loss)

    if verbose:
        (a, b) = closest_rectangle(len(hyperparameters) + 1)

        fig, axes = plt.subplots(b, a, constrained_layout=True, sharey=True,
                                 figsize=(4 * b, 3 * a))
        fig.suptitle(f"Hyperparameter Survey using {method_name} on {problem} problem.", fontsize="x-large", weight="bold")

        aa, bb = np.meshgrid(np.arange(a), np.arange(b))

        a_locs = np.array([[_a/aa.max(), _b/bb.max(), _c/(len(hyperparameters) - 1)] for (_a, _b, _c) in zip(aa.flatten(),
                                                                          bb.flatten(),
                                                                          np.arange(len(hyperparameters)))])

        axes = np.array(axes).flatten()

        hh, ll = [], []

        for hp, ax, a_loc in zip(hyperparameters, axes, a_locs):
            plt.sca(ax)
            hp.show_history(fig, ax, *a_loc)
            handles, labels = plt.gca().get_legend_handles_labels()
            hh.append(handles), ll.append(labels)

        hh, ll = np.array(hh, dtype=object).flatten(), np.array(ll, dtype=object).flatten()

        legend_axis = True
        for ax in axes:
            if not ax.collections:
                if legend_axis:
                    bbox = ax.get_position()
                    legend_axis = False
                ax.axis("off")

        # massive mess to get nice labeling for all axes
        by_label = dict(zip(hh, ll))
        clean_by_label = {}
        for key, value in by_label.items():
            if value not in clean_by_label.values() and key not in clean_by_label.keys():
                clean_by_label[key] = value
        fig.legend(clean_by_label.keys(), clean_by_label.values(), bbox_to_anchor=bbox,
                   loc="center", fontsize="large", ncol=2,
                   title="Legend", title_fontproperties={"weight": "bold",
                                                         "size": "x-large"})

        plt.savefig(f"./results/TPE-{problem}.png", dpi=400)

        plt.show()
        plt.clf()
    else:
        plt.clf()
        pass

    return history, configs


# UTIL FUNCTIONS
def closest_rectangle(n):
    a, b = 0, 0
    while a * b < n:
        a += 1
        if a * b >= n:
            break
        b += 1
    return a, b



###############################################################################################
# Code that parses command line arguments and saves the results
# code can be run by calling
# python3.9 hyperopt.py --algorithm tpe --problem good_range --more_arguments ...
# you do not need to change the code below
###############################################################################################

if __name__ == '__main__':
    alg_fn = {'rs': random_search, 'tpe': tpe}

    args = parser.parse_args()

    # using np.random.seed for dists etc is depreciated
    np.random.seed(args.seed)
    rng = np.random.default_rng(args.seed)

    conf = vars(args)
    tried_configs, performances = alg_fn[args.algorithm](**conf)
    if not os.path.isdir('./results'):
        os.mkdir('./results')
    savename = f"./results/{args.algorithm}-{args.problem}-{args.gamma}-{args.random_warmup}-{args.seed}-perfs.csv"

    df = pd.DataFrame(tried_configs)
    df["val_loss"] = performances
    df.to_csv(savename)
