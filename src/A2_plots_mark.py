import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import h5py

def main_task1_plot():
    fig, ax = plt.subplots(1, 1, constrained_layout=True, num="Task1")

    plt.sca(ax)

    labels = [
        "1","2","3","4","5"
    ]

    data_dirs = [
        "./results/1stmaml_num_ways20/1stmaml_default_num_ways20_1.h5",
        "./results/1stmaml_num_ways20/1stmaml_default_num_ways20_2.h5",
        "./results/1stmaml_num_ways20/1stmaml_default_num_ways20_3.h5",
    ]
    for i in range(len(data_dirs)):
        data_dirs[i] = Path(data_dirs[i])
    results = {
        "losses": np.full((len(data_dirs), 40000), fill_value=np.nan),
        "accuracies": np.full((len(data_dirs), 40000), fill_value=np.nan),
        "val_losses": np.full((len(data_dirs), int(40000/500), 1000), fill_value=np.nan),
        "val_accuracies": np.full((len(data_dirs), int(40000/500), 1000), fill_value=np.nan),
        "test_losses": np.full((len(data_dirs), 1000), fill_value=np.nan),
        "test_accuracies": np.full((len(data_dirs), 1000), fill_value=np.nan)
    }

    for i, (label, data_dir) in enumerate(zip(labels, data_dirs)):
        header, data = read_h5(data_dir)
        print(data["accuracies"].shape)
        for key, value in data.items():
            results[key][i] = data[key]

        print(header)

    save(results, header, name="1st_maml_num_ways20_full")

    plot_accuracy(results, label)
    # plot_losses(results, label)

    ax.set_ylim(0, None)

    plt.show()

def save(data, header, dir="./results", name=""):
    Path(dir).mkdir(parents=True, exist_ok=True)

    # even if the name is just one value (e.g.) gamma = 0.2,
    # internally there might be differences which get written into the header.
    # See the read_h5 function for an explanation

    file_name = Path(dir) / f"{name}.h5"

    try:
        f = h5py.File(file_name, 'w')

        for key, value in data.items():
            f.create_dataset(key, data=value)

        # Store metadata in hdf5 file
        for k in header.keys():
            f.attrs[k] = str(header[k])
        f.close()
    except RuntimeError:
        print(f"!! File could not be saved !!")
        print(name)
        raise RuntimeError

def read_h5(loc="NULL.h5"):
    """
    Gets the data from a specific experiment, returns all values in a dict
    Each row in the dict values is an experiment i.e. history.shape = (3, 150)
                                                        -> 3 experiments, 150 function evaluations
    The header has the corresponding experiment setup data in arrays that correspond to the rows of e.g. history
    """
    data = {}
    with h5py.File(loc, "r") as file:

        for key, val in file.items():
            data[key] = np.array(file[key])

        header = dict(file.attrs)

    return header, data

def read_result(dir):
    pass

def plot_losses(data, label, color=None, hatch=None, ls=None):
    fig = plt.gcf()
    ax = fig.gca()

    val_losses = data["val_losses"]
    test_losses = data["test_losses"]

    mean_val_losses = np.nanmean(val_losses, axis=(0, 1))
    mean_test_losses = np.nanmean(test_losses, axis=0)

    std_val_losses = np.nanstd(val_losses, axis=(0, 1))
    std_test_losses = np.nanstd(test_losses, axis=0)

    x = np.arange(len(mean_val_losses))

    ax.plot(x, mean_val_losses,
            label=label+r" $\hat{L}_{val}$",
            color=color,
            ls=ls)

    ax.fill_between(x,
                    mean_val_losses + 1 * std_val_losses,
                    mean_val_losses - 1 * std_val_losses,
                    label=label+r" $L_{val}$, 1 $\sigma$ CI",
                    color=color,
                    hatch=hatch,
                    alpha=0.3)

    ax.plot(x, mean_test_losses,
            label=label + r" $\hat{L}_{test}$",
            color=color,
            ls=ls)

    ax.fill_between(x,
                    mean_test_losses + 1 * std_test_losses,
                    mean_test_losses - 1 * std_test_losses,
                    label=label + r" $L_{test}$, 1 $\sigma$ CI",
                    color=color,
                    hatch=hatch,
                    alpha=0.3)
    ax.legend()
    return

def plot_accuracy(data, label, color=None, hatch=None, ls=None):
    ax = plt.gca()

    accuracies = data["accuracies"]

    mean_accuracies = np.mean(accuracies, axis=0)
    std_accuracies = np.std(accuracies, axis=0)


    x = np.arange(len(mean_accuracies))

    ax.plot(x, mean_accuracies,
            label=label+r" $\hat{Acc}$",
            color=color,
            ls=ls)

    ax.fill_between(x,
                    mean_accuracies + 1 * std_accuracies,
                    mean_accuracies - 1 * std_accuracies,
                    label=label+r" $\hat{Acc}$, 1 $\sigma$ CI",
                    color=color,
                    hatch=hatch,
                    alpha=0.3)
    ax.legend()
    return



if __name__ == '__main__':
    main_task1_plot()