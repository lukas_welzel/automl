import numpy as np
import matplotlib.pyplot as plt
from experiments import *
from scipy.signal import savgol_filter

def run_gamma_perf(steps=9, repeats=10):
    gammas = np.geomspace(0.01, 0.9, steps)
    seeds = rng.integers(0, 2_000_000_000, size=repeats * steps, dtype=int)

    setup = {
        "problem": 'good_range',
        "algorithm": 'tpe',
        "gamma": 0.1,
        "random_warmup": 10,
        "seed": 42,
        "verbose": False
    }

    setups = np.full(repeats * steps, fill_value=setup)

    for i in range(len(setups)):
        setups[i] = setup.copy()
        setups[i]["gamma"] = gammas[i % steps]
        setups[i]["seed"] = seeds[i]

    file_name = run_n_tpe_experiments(setups=setups, n=steps * repeats, name="GAMMA_PERF")
    return file_name

def plot_gamma_perf(loc="./results/GAMMA_PERF--tpe-good_range-0.1-10", steps=9, repeats=10):
    from scipy.signal import savgol_filter

    header, data = read_h5(loc=loc)

    losses = data["history"].reshape((repeats, steps, -1))
    gammas = header["gamma"]

    # oh my god, cursed
    gammas = np.fromstring(gammas.replace("\n", "").replace("[", ""). replace("]", ""), dtype=float, sep=" ")
    gammas = gammas.reshape((repeats, -1))

    mean_loss = np.nanmean(losses, axis=0)
    std_loss = np.std(losses, axis=0)

    fig, axes = plt.subplots(2, 2, constrained_layout=True,
                             gridspec_kw={"width_ratios":[0.25, 0.75],
                             "height_ratios":[0.75, 0.25]})

    hh, ll = [], []
    axes = np.array(axes)
    bbox = axes[1, 0].get_position()

    ax = axes[0, 1]
    ax_loss_avg = axes[0, 0]
    ax_gamma_avg = axes[1, 1]
    axes[1, 0].axis("off")

    ax.set_xscale("log")
    ax_loss_avg.sharey(ax)
    ax_gamma_avg.sharex(ax)
    ax_gamma_avg.set_xscale("log")


    smooth_mean_loss = np.array([savgol_filter(mean_loss[i], 21, 1) for i in range(len(mean_loss))])

    cont = ax.contourf(gammas[0], np.arange(mean_loss.shape[1], dtype=float), smooth_mean_loss.T)
    cbar = plt.colorbar(cont, ax=ax)
    cbar.ax.set_ylabel(r"$\hat{L}$")
    ax.set_ylabel("Function Evaluations")
    ax.set_xlabel(r"$\gamma$")



    ax_gamma_avg.plot(gammas[0], np.mean(mean_loss[:, 125:], axis=1), label=r"$\hat{L}$ (last 25 eval.)", ls="dashed",
                      color="darkblue")
    ax_gamma_avg.fill_between(gammas[0],
                              np.mean(mean_loss[:, 125:], axis=1) + 6 * np.std(mean_loss[:, 125:], axis=1),
                              np.mean(mean_loss[:, 125:], axis=1) - 6 * np.std(mean_loss[:, 125:], axis=1),
                              label=r"6 $\sigma$ CI (last 25 eval.)",
                              color="gray",
                              alpha=0.5)
    handles, labels = plt.gca().get_legend_handles_labels()
    hh.append(handles), ll.append(labels)
    ax_gamma_avg.set_ylabel(r"$\hat{L}$")
    ax_gamma_avg.xaxis.tick_top()
    custom_ticks = np.linspace(0, 6, 3, dtype=int)
    ax_gamma_avg.set_yticks(custom_ticks)
    ax_gamma_avg.set_yticklabels(custom_ticks)





    ax_loss_avg.plot(np.mean(mean_loss, axis=0), np.arange(mean_loss.shape[1]), label=r"$\hat{L}$",
                     ls="dotted", color="darkred", alpha=0.8)
    # ax_loss_avg.plot(savgol_filter(np.mean(mean_loss, axis=0), 21, 1), np.arange(mean_loss.shape[1]),
    #                  label=r"Smooth $\hat{L}$", ls="-.", color="orange")
    ax_loss_avg.fill_betweenx(np.arange(mean_loss.shape[1]),
                              np.mean(mean_loss, axis=0) + 1 * np.std(mean_loss, axis=0),
                              np.mean(mean_loss, axis=0) - 1 * np.std(mean_loss, axis=0),
                              label=r"1 $\sigma$ CI",
                              color="gray",
                               hatch="/",
                              alpha=0.5)
    plt.sca(ax_loss_avg)
    handles, labels = plt.gca().get_legend_handles_labels()
    hh.append(handles), ll.append(labels)
    ax_loss_avg.yaxis.tick_right()
    ax_loss_avg.set_xlabel(r"$\hat{L}$")
    ax_loss_avg.set_xticks(custom_ticks)
    ax_loss_avg.set_xticklabels(custom_ticks)

    plt.setp(ax_gamma_avg.get_xticklabels(), visible=False)
    plt.setp(ax_loss_avg.get_yticklabels(), visible=False)


    hh, ll = np.array(hh, dtype=object).flatten(), np.array(ll, dtype=object).flatten()

    try:
        _hh = []
        for li in hh:
            for h in li:
                _hh.append(h)
        _ll = []
        for li in ll:
            for l in li:
                _ll.append(l)
        by_label = dict(zip(_hh, _ll))
    except TypeError:
        by_label = dict(zip(hh, ll))

    clean_by_label = {}
    for key, value in by_label.items():
        if value not in clean_by_label.values() and key not in clean_by_label.keys():
            clean_by_label[key] = value
    fig.legend(clean_by_label.keys(), clean_by_label.values(),
               loc="lower left", fontsize="x-small", ncol=1,
               title="Legend", title_fontproperties={"weight": "bold",
                                                     "size": "small"})

    fig.suptitle(r"$\gamma$ Survey (N=20, 10 steps random warmup)")

    plt.savefig("./results/gamma_survey.png", dpi=400)

    plt.show()


def run_TPE_survey(repeats, problem='good_range'):
    seeds = rng.integers(0, 2_000_000_000, size=repeats, dtype=int)
    setup = {
        "problem": problem,
        "algorithm": 'tpe',
        "gamma": 0.1,
        "random_warmup": 10,
        "seed": 42,
        "verbose": False
    }
    setups = np.full(repeats, fill_value=setup)

    for i in range(len(setups)):
        setups[i] = setup.copy()
        setups[i]["seed"] = seeds[i]

    file_name = run_n_tpe_experiments(setups=setups, n=repeats, name=f"TPE_SURVEY")
    return file_name

def plot_TPE_survey(loc="./results/TPE_SURVEY--tpe-good_range-0.1-10.h5", repeats=10):
    from scipy.signal import savgol_filter

    header, data = read_h5(loc=loc)

    print(data.keys())


def run_RS_survey(repeats, problem='good_range'):
    seeds = rng.integers(0, 2_000_000_000, size=repeats, dtype=int)
    setup = {
        "problem": problem,
        "algorithm": 'rs',
        "gamma": 0.1,
        "random_warmup": 10,
        "seed": 42,
        "verbose": False
    }
    setups = np.full(repeats, fill_value=setup)

    for i in range(len(setups)):
        setups[i] = setup.copy()
        setups[i]["seed"] = seeds[i]

    file_name = run_n_tpe_experiments(setups=setups, n=repeats, name=f"RS_SURVEY")
    return file_name



def tpe_experiments():
    file_name = run_TPE_survey(10)
    plot_TPE_survey(loc="./results/TPE_SURVEY--tpe-good_range-0.1-10.h5")


def tpe_rs_comp(tpe_file="./results/TPE_SURVEY--tpe-good_range-0.1-10.h5",
                rs_file="./results/RS_SURVEY--rs-good_range-0.1-10.h5"):
    from scipy.signal import savgol_filter

    tpe_header, tpe_data = read_h5(loc=tpe_file)
    rs_header, rs_data = read_h5(loc=rs_file)

    tpe_history = tpe_data["history"]
    rs_history = rs_data["history"]

    tpe_mean_loss = np.mean(tpe_history, axis=0)
    tpe_std_loss = np.std(tpe_history, axis=0)

    rs_mean_loss = np.mean(rs_history, axis=0)
    rs_std_loss = np.std(rs_history, axis=0)

    fig, ax = plt.subplots(1, 1, constrained_layout=True)

    x = np.arange(len(tpe_mean_loss))

    ax.plot(x, tpe_mean_loss, label=r"TPE", ls="solid", color="darkblue")
    ax.axvline(10, label="End of warmup", color="black", ls="dotted")
    ax.fill_between(x,
                    tpe_mean_loss + 1 * tpe_std_loss,
                    tpe_mean_loss - 1 * tpe_std_loss,
                    label=r"TPE 1 $\sigma$ CI",
                    color="lightblue",
                    alpha=0.3)
    ax.axhline(np.mean(tpe_mean_loss[-10:]), color="gray", lw=0.5)

    ax.plot(x, rs_mean_loss, label=r"RS", ls="dashed", color="darkred")
    ax.plot(x, savgol_filter(rs_mean_loss, 31, 1), label=r"Smooth RS", ls="-.", color="red")

    ax.fill_between(x,
                    rs_mean_loss + 1 * rs_std_loss,
                    rs_mean_loss - 1 * rs_std_loss,
                    label=r"RS 1 $\sigma$ CI",
                    color="crimson",
                    hatch="/",
                    alpha=0.3)

    ax.axhline(np.mean(rs_mean_loss[-10:]), color="gray", lw=0.5)

    plt.legend(ncol=2)

    ax.set_ylabel(r"$\hat{L}$")
    ax.set_xlabel(r"Function Evaluations")

    fig.suptitle("TPE-RS Comparison (N=20)")

    plt.savefig("./results/TPE-RS-comparison.png", dpi=400)

    plt.show()


def gamma_experiment():
    steps = 20
    repeats = 20
    try:
        plot_gamma_perf(loc="./results/GAMMA_PERF--tpe-good_range-0.01-10.h5", steps=steps, repeats=repeats)
    except FileNotFoundError:
        file_name = run_gamma_perf(steps=steps, repeats=repeats)
        plot_gamma_perf(loc=file_name, steps=steps, repeats=repeats)


def smallest_before(data):
    data[np.isnan(data)] = 0.0
    _data = np.zeros_like(data)
    for i, row in enumerate(data):
        m = row[0]
        for j, el in enumerate(row):
            m = np.minimum(m, row[j])
            _data[i, j] = m

    return _data


def plot_perf_evals(ax, data, label, color, hatch=None, ls=None):
    window = 5

    # print(data.shape)

    data = smallest_before(data)

    mean_loss = np.nanmean(data, axis=0)
    smooth_mean_loss = savgol_filter(mean_loss, window, 1)
    std_loss = np.nanstd(data, axis=0)
    smooth_std_loss = savgol_filter(std_loss, window, 1)

    x = np.arange(len(smooth_mean_loss))

    ax.plot(x, smooth_mean_loss, label=label, ls=ls ,color=color)

    ax.fill_between(x,
                    smooth_mean_loss + 1 * smooth_std_loss,
                    smooth_mean_loss - 1 * smooth_std_loss,
                    # label=r"TPE 1 $\sigma$ CI",
                    color=color,
                    hatch=hatch,
                    alpha=0.1)

    # for d in data:
    #     ax.plot(x, savgol_filter(d, window, 1), ls=ls, color=color, alpha=0.1, lw=0.5)



def new_a1_t2_tasks(tpe_interactive="./results/new_survey--tpe-interactive-0.1-10.h5",
                    tpe_good="./results/new_survey--tpe-good_range-0.1-10.h5",
                    tpe_bad="./results/new_survey--tpe-bad_range-0.1-10.h5",
                    rs_interactive="./results/new_survey--rs-interactive-0.1-10.h5",
                    rs_good="./results/new_survey--rs-good_range-0.1-10.h5",
                    rs_bad="./results/new_survey--rs-bad_range-0.1-10.h5"):
    from scipy.signal import savgol_filter

    tpe_header_interactive, tpe_data_interactive = read_h5(loc=tpe_interactive)
    tpe_header_good, tpe_data_good = read_h5(loc=tpe_good)
    tpe_header_bad, tpe_data_bad = read_h5(loc=tpe_bad)

    rs_header_interactive, rs_data_interactive = read_h5(loc=rs_interactive)
    rs_header_good, rs_data_good = read_h5(loc=rs_good)
    rs_header_bad, rs_data_bad = read_h5(loc=rs_bad)

    tpe_history_interactive = tpe_data_interactive["history"]
    tpe_history_good = tpe_data_good["history"]
    tpe_history_bad = tpe_data_bad["history"]

    rs_history_interactive = rs_data_interactive["history"]
    rs_history_good = rs_data_good["history"]
    rs_history_bad = rs_data_bad["history"]

    # print(tpe_header_good)
    # print("\n")
    # print(tpe_history_good.shape)

    datas = [
        tpe_history_good,
        tpe_history_bad,
        rs_history_good,
        rs_history_bad,
        tpe_history_interactive,
        rs_history_interactive,
    ]
    labels = [
        "TPE Good",
        "TPE Bad",
        "RS Good",
        "RS Bad",
        "TPE Interactive",
        "RS Interactive",
    ]
    colors = [
        "lime",
        "cyan",
        "red",
        "orange",
        "green",
        "darkred",
    ]
    hatches = [
        "x",
        "//",
        "x",
        "//",
        "\\",
        "\\",
    ]
    lss = [
        "solid",
        "dashed",
        "solid",
        "dashed",
        "dotted",
        "dotted",
    ]

    fig, (ax, ax2) = plt.subplots(1, 2, constrained_layout=True, figsize=(15, 5))

    ax.axvline(10, label="End of warmup", color="black", ls="dotted")
    ax2.axvline(10, color="black", ls="dotted")

    for runs, label, color, hatch, ls in zip(datas[:-2], labels, colors, hatches, lss):
        plot_perf_evals(ax, runs, label, color, hatch=hatch, ls=ls)

    for runs, label, color, hatch, ls in zip(datas[-2:], labels[-2:], colors[-2:], hatches[-2:], lss[-2:]):
        plot_perf_evals(ax2, runs, label, color, hatch=hatch, ls=ls)

    ax.set_yscale("linear")
    ax.legend(ncol=2)

    ax2.set_yscale("symlog")
    ax2.legend(ncol=2)
    # ax2.set_ylim(None, 0.)

    ax.set_ylabel(r"$\left(\hat{L}\right)_{min}$")
    ax.set_xlabel(r"Function Evaluations")

    ax2.set_ylabel(r"$\left(\hat{L}\right)_{min}$")
    ax2.set_xlabel(r"Function Evaluations")

    fig.suptitle("TPE & RS Survey over problems (N=50)")

    plt.savefig("./results/all-comparison.png", dpi=400)

    plt.show()



if __name__ == '__main__':
    # run_RS_survey(20)
    # run_TPE_survey(20)
    # gamma_experiment()
    # tpe_experiments()
    # tpe_rs_comp()
    # run_boring_experiments()
    new_a1_t2_tasks()