#########################################################################################
# The assignment file. While it is allowed to change this entire file, we highly
# recommend using the provided template. YOU MUST USE THE RANGES AND HYPERPARAMETERS SPECIFIED
# IN GET_RANGES AND GET_CONFIG_PERFORMAMCE (IN SHORT: USE OUR SURROGATE PROBLEMS)
#########################################################################################

import numpy as np
import argparse
import os
import pandas as pd
import matplotlib.pyplot as plt
from utils import normal_dist

from utils import GET_CONFIG_PERFORMANCE, GET_RANGES, SampleType, \
    ParamType  # make sure to make use of ParamType and SampleType in your code

parser = argparse.ArgumentParser()
parser.add_argument('--problem', choices=['good_range', 'bad_range', 'interactive'],
                    default='good_range')  # required=False)
parser.add_argument('--algorithm', choices=['rs', 'tpe'], default="tpe")  # required=False)
parser.add_argument('--gamma', type=float, default=0.2)
parser.add_argument('--random_warmup', type=int, default=30)
parser.add_argument('--seed', type=int, default=42)

rng = np.random.default_rng()

class CustomDistribution:
    """
    Based on https://stackoverflow.com/questions/29095070/how-to-simulate-from-an-arbitrary-continuous-probability-distribution
    """
    def __init__(self, x, pdf, distr, **kwargs):
        self.x = x
        self.pdf = pdf
        self.distr = distr

    def update(self, pdf):
        self.pdf = pdf

    def sample(self, n, **kwargs):
        cumpdf = np.cumsum(self.pdf)
        cumpdf *= 1 / cumpdf[-1]

        unif_samples = self.distr(size=n, **kwargs)

        ind1 = np.searchsorted(cumpdf, unif_samples)

        ind0 = np.where(ind1 == 0, 0, ind1 - 1)
        ind1[ind0 == 0] = 1

        frac1 = (unif_samples - cumpdf[ind0]) / (cumpdf[ind1] - cumpdf[ind0])
        samples = self.x[ind0] * (1 - frac1) + self.x[ind1] * frac1
        return samples

class HyperPara(ParamType, SampleType):
    """
    (Sub)Class this data holder for convenience.
    """
    Cat = ParamType.Categorical
    Int = ParamType.Int
    Real = ParamType.Real

    Uniform = SampleType.Uniform
    LogUniform = SampleType.LogUniform

    def __init__(self, name, para_type, sample_type, interval=None, dist_resolution=100, function_evaluations=20,
                 gamma=0.2, condition=None, **kwargs):
        super(HyperPara, self).__init__()

        self.name = name

        self.para_type = para_type
        self.sample_type = sample_type
        if condition is None:
            self.condition = self.eval_condition
        else:
            self.condition = condition

        self.distribution = np.full(shape=(function_evaluations, dist_resolution), fill_value=np.nan)
        self.sample_history = np.full(function_evaluations, fill_value=np.nan)
        self.compliance = np.full_like(self.sample_history, fill_value=False)
        self._n_samples = 0
        self.losses = np.full(function_evaluations, fill_value=np.nan)
        self.good_bad = np.full(function_evaluations, fill_value=False)  # False = bad, True = good
        self.gamma = gamma
        self.dist_good = 0
        self.dist_bad = 0

        if self.para_type == self.__class__.Cat:  # CATEGORICAL
            # to use np arrays we need to represent the categorical choices as ints/float
            self.num_interval = np.arange(1, len(interval)+1, dtype=int)
            self.interval = interval
            self._sample = self._sample_categorical
            self.__PARATYPE = "Categorical"
            self.__SAMPLETYPE = "Categorical"
            self.x = interval
            self.distribution = np.zeros_like(interval)
        else:  # NUMBERS
            if self.para_type == self.__class__.Int:  # INTEGERS
                self.distr = rng.integers
                self.interval = interval
                self.__PARATYPE = "Integer"
                self.__wanted_dtype = int
            elif self.para_type == self.__class__.Real:  # REAL
                self.distr = rng.uniform
                self.interval = interval
                self.__PARATYPE = "Real"
                self.__wanted_dtype = float

            if self.sample_type == self.__class__.Uniform:  # SAMPLING
                self._sample = self._sample_uniform
                self.__SAMPLETYPE = "Uniform"
                self.x = np.linspace(*interval, dist_resolution)
            elif self.sample_type == self.__class__.LogUniform:
                self.distr = rng.uniform
                self._sample = self._sample_log_uniform
                self.__SAMPLETYPE = "LogUniform"
                self.x = np.geomspace(interval[0], interval[1], dist_resolution, dtype=self.__wanted_dtype)

        self.dist_expected_improvement = np.ones_like(self.x) / len(self.x)

        self.__sample_distribution = CustomDistribution(self.x,
                                                        pdf=self.dist_expected_improvement,
                                                        distr=self.distr)

    def __str__(self):
        s = f"===============================\n" \
            f"Name:     {self.name}\n" \
            f"Type:     {self.__PARATYPE}\n" \
            f"Sample:   {self.__SAMPLETYPE}\n" \
            f"From:     {self.interval}"
        return s

    def eval_condition(self, x):
        return True

    def sample(self, size=1, **kwargs):
        if self.para_type == self.__class__.Real:
            value = self.__sample_distribution.sample(size, **kwargs)
        else:
            value = self._sample(size, *kwargs)
        self.sample_history[self._n_samples:self._n_samples + size] = value
        self._n_samples += size
        return value

    def set_loss(self, loss):
        self.losses[self._n_samples - len(loss):self._n_samples] = loss

    def _sample_uniform(self, size=1, low=None, high=None):
        if low is None:
            low = np.min(self.interval)
        if high is None:
            high = np.max(self.interval)

        dis = self.distr(low=low,
                         high=high,
                         size=size, )
        return dis

    def _sample_log_uniform(self, size=1, base=10.):
        dis = np.power(base, self._sample_uniform(size=size,
                                                  low=np.log10(np.min(self.interval), dtype=float),
                                                  high=np.log10(np.max(self.interval), dtype=float)))

        dis = dis.astype(self.__wanted_dtype)
        return dis

    def _sample_categorical(self, size=1, replace=True, p=None):
        return rng.choice(self.num_interval, size=size, replace=replace, p=self.dist_expected_improvement)

    def update_distribution(self):
        # removes all non-compliant data points (based on condition) from the hyperparameter history
        self.enforce_compliance()

        # TODO: now its only the largest, but do we want the smallest losses, or in absolute value?
        # below is the old solution, I though we can get away with using partitioning
        # (this is faster, doesnt sort, just guarantees that the smallest n (no specific order) are known)
        # because sigma varies for each sample this is not efficient and sorting the array will give better performance
        # OLD CODE
        n_smallest = int(np.ceil(self.losses.size * self.gamma))
        # ind_good = np.argpartition(self.losses, n_smallest)[:n_smallest]
        # self.good_bad[ind_good] = True
        # NEW CODE
        ind_sorted = np.argsort(self.losses)
        # retain this even though its no longer needed for posterity
        self.good_bad[ind_sorted[:n_smallest]] = True

        self.losses = self.losses[ind_sorted]
        self.good_bad = self.good_bad[ind_sorted]
        self.sample_history = self.sample_history[ind_sorted]
        self.compliance = self.compliance[ind_sorted]


        # TODO these distributions are not truncated properly! Still need to do that using the interval (shift down)
        if self.para_type == self.__class__.Categorical:
            # this is kinda weird now that we sort the array, but it should not be significantly slower
            unique_good, counts_good = np.unique(self.sample_history[self.good_bad], return_counts=True)
            unique_bad, counts_bad = np.unique(self.sample_history[~self.good_bad], return_counts=True)

            self.dist_good = np.full_like(self.interval, fill_value=0., dtype=float)
            self.dist_bad = np.full_like(self.interval, fill_value=0., dtype=float)

            # # TODO: currently assumes that each entry has good & bad entries, if that is not the case this will fail
            # assert unique_good.size == len(self.interval), f"Not all entries have good data losses.\n" \
            #                                                f"HP name:  {self.name}\n" \
            #                                                f"Interval: {self.interval}\n" \
            #                                                f"Goods:    {unique_good}, counts: {counts_good}\n" \
            #                                                f"Bads:     {unique_bad}, counts: {counts_bad}"
            # assert unique_bad.size == len(self.interval), f"Not all entries have bad data losses.\n" \
            #                                                f"HP name:  {self.name}\n" \
            #                                                f"Interval: {self.interval}\n" \
            #                                                f"Goods:    {unique_good}, counts: {counts_good}\n" \
            #                                                f"Bads:     {unique_bad}, counts: {counts_bad}"

            # TODO: attempt to fix above issue
            for goods, count in zip(unique_good, counts_good):
                self.dist_good[int(goods) - 1] = count / self.losses.size
            for bads, count in zip(unique_bad, counts_bad):
                self.dist_bad[int(bads) - 1] = count / self.losses.size

        else:

            # Take max difference of neighbours
            diff_right = np.zeros_like(self.sample_history)
            diff_right[1:] = np.abs(self.sample_history[:-1] - self.sample_history[1:])
            diff_left = np.zeros_like(self.sample_history)
            diff_left[:-1] = np.abs(self.sample_history[1:] - self.sample_history[:-1])

            sigmas = np.maximum(diff_right, diff_left)

            # There can be samples that are on top of each other, i.e. their distance is zero.
            # We find these by finding zero distances in above array
            # This doesnt really happen (often) but better to be safe than sorry
            # It happened and caused an issue counter: 1    # increase if it causes issues again

            zero_dist_ind = np.argwhere(sigmas == 0)
            get_neighbours = lambda x: np.array([x-1, x+1])  # only for flat arrays
            for z in zero_dist_ind:
                neighbours = get_neighbours(z)
                try:
                    while sigmas[neighbours[1]] == 0:  # what a shit show,
                        # why am I even checking for multiple zero distance samples...
                        # I guess it makes sense for non-categorical integers
                        neighbours[1] = + 1
                    sigmas[z] = np.max(sigmas[neighbours])
                except IndexError:
                    try:
                        left = sigmas[neighbours[0]]
                    except IndexError:
                        left = 0
                    try:
                        right = sigmas[neighbours[1]]
                    except IndexError:
                        right = 0

                    sigmas[z] = np.max(left, right)

            for i, loss in enumerate(self.losses):
                self.distribution[i] = normal_dist(self.x, self.sample_history[i], sigmas[i])

            self.dist_good = np.nanmean(self.distribution[self.good_bad], axis=0)  # / n_smallest
            self.dist_bad = np.nanmean(self.distribution[~self.good_bad], axis=0)  # / (self.losses.size - n_smallest)

        maximum = np.max(self.dist_good + self.dist_bad)

        self.dist_good = self.dist_good / maximum
        self.dist_bad = self.dist_bad / maximum
        # TODO: this can lead to undefined EI, if for one option no samples are 'bad'
        self.dist_expected_improvement = self.dist_good / self.dist_bad
        # TODO: They are all PDFs so they must be normalized properly!
        self.dist_expected_improvement = self.dist_expected_improvement / np.max(self.dist_expected_improvement)

    def tpe_predict(self, n, logits=False, refine=10):
        if logits:
            return self.dist_expected_improvement, self.x
        else:
            return self.x[np.argmax(self.dist_expected_improvement)]

    def show_history(self, fig, ax, a, b, c, losses=None, color="black"):
        # fig, ax = plt.subplots(1, 1, constrained_layout=True)

        if losses is None:
            losses = self.losses
        else:
            losses = losses[self.compliance]

        _loss = losses  # np.sqrt(np.square(losses))

        y_star = np.mean([np.max(_loss[self.good_bad]),
                          np.min(_loss[~self.good_bad])])
        ax.axhline(y_star, c="gray", ls=(0, (5, 10)), label=r"$y^{\star}$")

        ax.scatter(self.sample_history[self.good_bad], _loss[self.good_bad],
                   marker="D", s=8, color="darkblue", alpha=0.3, zorder=0)
        ax.scatter(self.sample_history[~self.good_bad], _loss[~self.good_bad],
                   marker=".", s=8, color="darkred", alpha=0.3, zorder=0)

        # ax.set_title(f"{self.name} Survey")
        ax.set_xlabel(f"{self.name}")

        if self.sample_type == self.__class__.LogUniform:
            ax.set_xscale("log")

        if self.para_type == self.__class__.Categorical:
            width = 0.225

            unique_samples = np.array([np.where(self.sample_history == v)[0] for v in np.unique(self.sample_history)],
                                      dtype=object)
            data = np.array([_loss[i] for i in unique_samples],
                            dtype=object)

            ax.boxplot(data, positions=self.num_interval, notch=True, showfliers=False, widths=width * 0.8,
                       zorder=99)


            ax_dist = ax.twinx()

            ax_dist.bar(self.num_interval - 2 * width * 1.05, self.dist_good, width=width,
                        label=r"Good [$l(x)$] ", alpha=0.9, hatch="/", color="lightblue")
            ax_dist.bar(self.num_interval - width * 1.05, self.dist_bad, width=width,
                        label=r"Bad   [$g(x)$] ", alpha=0.9, hatch="\\", color="crimson")
            ax_dist.bar(self.num_interval + width * 1.05, self.dist_expected_improvement, width=width,
                        label=r"EI      [$l(x)/g(x)$] ", alpha=0.9, hatch="o", color="darkgreen")

            ax.set_xticks(self.num_interval, self.interval)



        else:
            ax_dist = ax.twinx()
            ax_dist.plot(self.x, self.dist_good,
                         label=r"Good [$l(x)$]", ls="dashed", lw=2., color="lightblue")
            ax_dist.plot(self.x, self.dist_bad,
                         label=r"Bad   [$g(x)$]", ls="dotted", lw=2., color="crimson")
            ax_dist.plot(self.x, self.dist_expected_improvement,
                         label=r"EI      [$l(x) / g(x)$]", ls="solid", lw=2., color="darkgreen")

        if a == 0:
            ax.set_ylabel(r"Loss [-]")

        if a == 1 or c == 1:
            ax_dist.set_ylabel("Distribution [-]")


        # ax.legend()
        # ax_dist.legend()
        # plt.show()

        return ax

    def enforce_compliance(self):
        try:
            self.distribution = self.distribution[self.compliance]
        except IndexError:
            # this means that we are trying to remove samples from the self.distribution of a categorical,
            # which doesnt exist in this implementation dim(self.distribution ) = dim(interval) for categoricals
            pass

        self.sample_history = self.sample_history[self.compliance]
        self.good_bad = self.good_bad[self.compliance]
        self.losses = self.losses[self.compliance]
        # TODO: I removed these to make sure I can continue to interact with externals etc
        # TODO: I am still not sure if the better way is to just keep all values and just use the ones that comply in the eval steps
        # self.compliance = self.compliance[self.compliance]
        # self._n_samples = len(self.sample_history)


class TestCase(object):
    def __init__(self, hyperparameters):
        super(TestCase, self).__init__()

        self.hps = np.array(hyperparameters, dtype=object)

        self._names = np.array([hp.name for hp in self.hps])

    def __str__(self):
        s = "\n".join([hp.__str__() for hp in self.hps])
        s = "/////////// CONFIG  ///////////\n" + s + "\n///////////////////////////////"
        return s

    def sample(self, n=1):
        for i, hp in enumerate(self.hps):
            hp.__sample_distribution.update(pdf=hp.dist_expected_improvement)

        if n == 1:
            return dict(np.array([(hp.name, hp.sample()) for hp in self.hps], dtype=object))

        # so this is a bit dangerous; the array casts to float, even if some options are int.
        # as long as internal converters can handle the translation we are fine ;)
        arr = np.full(shape=(self.hps.size, n), fill_value=np.nan)
        for i, hp in enumerate(self.hps):
            arr[i] = hp.sample(size=n)

        arr = np.array([dict(zip(self._names, arr[:, j])) for j in np.arange(arr[0].size)])

        # while not needed I enforce condition compliance for a config.
        # here it is ok since the eval function in utils accepts the input,
        # however for real functions that would typically not be the case
        self.check_conditions(arr)
        arr = self.enforce_compliance(arr)

        return arr

    def check_conditions(self, arr):
        # the implementation in utils is truely unholy, makes vectorization impossible

        for hp in self.hps:
            compliance = np.array([hp.condition(el) for el in arr])
            hp.compliance = compliance

    def enforce_compliance(self, arr):
        for i, config in enumerate(arr):
            for hp in self.hps:
                if ~hp.compliance[i]:
                    del arr[i][hp.name]

        return arr

    def set_losses(self, loss):
        for hp in self.hps:
            hp.set_loss(loss)


def random_search(problem, function_evaluations=150, **kwargs):
    """
    Function that performs random search on the given problem. It uses the
    ranges and sampling types defined in GET_RANGES(problem) (see utils.py).

    Arguments:
      - problem (str): the prolbem identifier
      - function_evaluations (int): the number of configurations to evaluate
      - **kwargs: any other keyword arguments

    Returns:
      - history (list): A list of the observed losses
      - configs (list): A list of the tried configurations. Every configuration is a dictionary
                        mapping hyperparameter names to the chosen values
    """
    method_name = "Random Search"

    history = np.full(shape=function_evaluations, fill_value=np.nan)
    # configs = np.full(shape=n, fill_value={})

    # get all information about the hyperparameters we want to tune for this problem
    # (see utils.py) for the form of this.
    RANGES = GET_RANGES(problem)

    hyperparameters = np.array([HyperPara(name=key,
                                          para_type=values["type"],
                                          sample_type=values["sample"],
                                          interval=values["range"],
                                          function_evaluations=function_evaluations,
                                          condition=values.get("condition", None),
                                          **kwargs)
                                for (key, values) in RANGES.items()
                                ], dtype=object)
    case = TestCase(hyperparameters)

    # print(case)

    configs = case.sample(n=function_evaluations)

    # print(configs)

    for i, sc in enumerate(configs):
        loss = GET_CONFIG_PERFORMANCE(sc, problem=problem)
        history[i] = loss

    case.set_losses(loss=history)

    (a, b) = closest_rectangle(len(hyperparameters) + 1)

    fig, axes = plt.subplots(b, a, constrained_layout=True, sharey=True,
                             figsize=(7 * b, 5 * a))
    fig.suptitle(f"Hyperparameter Survey using {method_name}", fontsize="xx-large", weight="bold")

    aa, bb = np.meshgrid(np.arange(a), np.arange(b))

    a_locs = np.array(
        [[_a / aa.max(), _b / bb.max(), _c / (len(hyperparameters) - 1)] for (_a, _b, _c) in zip(aa.flatten(),
                                                                                                 bb.flatten(),
                                                                                                 np.arange(
                                                                                                     len(hyperparameters)))])

    axes = np.array(axes).flatten()

    hh, ll = [], []

    for hp, ax, a_loc in zip(hyperparameters, axes, a_locs):
        # hp.update_distribution()
        hp.show_history(fig, ax, *a_loc)
        handles, labels = plt.gca().get_legend_handles_labels()
        hh.append(handles), ll.append(labels)

    hh, ll = np.array(hh, dtype=object).flatten(), np.array(ll, dtype=object).flatten()

    legend_axis = True
    for ax in axes:
        if not ax.collections:
            if legend_axis:
                bbox = ax.get_position()
                legend_axis = False
            ax.axis("off")

    # massive mess to get nice labeling for all axes
    by_label = dict(zip(hh, ll))
    clean_by_label = {}
    for key, value in by_label.items():
        if value not in clean_by_label.values() and key not in clean_by_label.keys():
            clean_by_label[key] = value
    fig.legend(clean_by_label.keys(), clean_by_label.values(), bbox_to_anchor=bbox,
               loc="center", fontsize="large", ncol=2,
               title="Legend", title_fontproperties={"weight": "bold",
                                                     "size": "x-large"})

    plt.show()

    return history, configs


def tpe(problem, function_evaluations=1500, random_warmup=30, gamma=0.2, **kwargs):
    """
    Function that uses Tree Parzen Estimator (TPE) to tune the hyperparameters of the
    given problem. It uses the ranges and sampling types defined in GET_RANGES(problem)
    (see utils.py).

    Arguments:
      - problem (str): the prolbem identifier
      - function_evaluations (int): the number of configurations to evaluate
      - random_warmup (int): the number of initial iterations during which we perform random
                             search
      - gamma: the value of gamma that determines the cutting point [good partition, bad partition]
      - **kwargs: any other keyword arguments

    Returns:
      - history (list): A list of the observed losses
      - configs (list): A list of the tried configurations. Every configuration is a dictionary
                        mapping hyperparameter names to the chosen values
    """

    # TODO: losses need to be assigned before update_distribution() is called
    method_name = "TPE"

    history = np.full(shape=function_evaluations, fill_value=np.nan)
    # configs = np.full(shape=n, fill_value={})

    # get all information about the hyperparameters we want to tune for this problem
    # (see utils.py) for the form of this.
    RANGES = GET_RANGES(problem)

    hyperparameters = np.array([HyperPara(name=key,
                                          para_type=values["type"],
                                          sample_type=values["sample"],
                                          interval=values["range"],
                                          function_evaluations=function_evaluations,
                                          gamma=gamma,
                                          condition=values.get("condition", None),
                                          **kwargs)
                                for (key, values) in RANGES.items()
                                ], dtype=object)
    case = TestCase(hyperparameters)

    # print(case)

    configs = case.sample(n=function_evaluations)

    # print(configs)

    for i, sc in enumerate(configs):
        loss = GET_CONFIG_PERFORMANCE(sc, problem=problem)
        history[i] = loss


    for hp in hyperparameters:
        hp.update_distribution()


    case.set_losses(loss=history)

    (a, b) = closest_rectangle(len(hyperparameters) + 1)

    fig, axes = plt.subplots(b, a, constrained_layout=True, sharey=True,
                             figsize=(7 * b, 5 * a))
    fig.suptitle(f"Hyperparameter Survey using {method_name}", fontsize="xx-large", weight="bold")

    aa, bb = np.meshgrid(np.arange(a), np.arange(b))

    a_locs = np.array([[_a/aa.max(), _b/bb.max(), _c/(len(hyperparameters) - 1)] for (_a, _b, _c) in zip(aa.flatten(),
                                                                      bb.flatten(),
                                                                      np.arange(len(hyperparameters)))])

    axes = np.array(axes).flatten()

    hh, ll = [], []

    for hp, ax, a_loc in zip(hyperparameters, axes, a_locs):
        hp.update_distribution()
        hp.show_history(fig, ax, *a_loc)
        handles, labels = plt.gca().get_legend_handles_labels()
        hh.append(handles), ll.append(labels)

    hh, ll = np.array(hh, dtype=object).flatten(), np.array(ll, dtype=object).flatten()

    legend_axis = True
    for ax in axes:
        if not ax.collections:
            if legend_axis:
                bbox = ax.get_position()
                legend_axis = False
            ax.axis("off")

    # massive mess to get nice labeling for all axes
    by_label = dict(zip(hh, ll))
    clean_by_label = {}
    for key, value in by_label.items():
        if value not in clean_by_label.values() and key not in clean_by_label.keys():
            clean_by_label[key] = value
    fig.legend(clean_by_label.keys(), clean_by_label.values(), bbox_to_anchor=bbox,
               loc="center", fontsize="large", ncol=2,
               title="Legend", title_fontproperties={"weight": "bold",
                                                     "size": "x-large"})

    plt.show()

    return history, configs


# UTIL FUNCTIONS
def closest_rectangle(n):
    a, b = 0, 0
    while a * b < n:
        a += 1
        if a * b >= n:
            break
        b += 1
    return a, b



###############################################################################################
# Code that parses command line arguments and saves the results
# code can be run by calling 
# python3.9 hyperopt.py --algorithm tpe --problem good_range --more_arguments ...
# you do not need to change the code below
###############################################################################################

if __name__ == '__main__':
    alg_fn = {'rs': random_search, 'tpe': tpe}

    args = parser.parse_args()

    np.random.seed(args.seed)

    conf = vars(args)
    tried_configs, performances = alg_fn[args.algorithm](**conf)
    if not os.path.isdir('../results'):
        os.mkdir('../results')
    savename = f"./results/{args.algorithm}-{args.problem}-{args.gamma}-{args.random_warmup}-{args.seed}-perfs.csv"

    df = pd.DataFrame(tried_configs)
    df["val_loss"] = performances
    df.to_csv(savename)
