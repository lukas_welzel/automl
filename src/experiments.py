# We use this to run experiments on TPE and RS. Both TPE and RS are written using only the provided modules.
# In the interest of our own time we use multiprocessing here to speed up the experiments.

import numpy as np
try:
    import h5py
    from pathlib import Path
except ModuleNotFoundError:
    raise NotImplementedError("We will not use csvs for large stacks of data.")
try:
    from pathos.pools import ProcessPool
    from pathos.helpers import cpu_count
except ModuleNotFoundError:
    print("Depending on progress this experiment might require the 'pathos' multiprocessing module fork.\n"
          "If it is not installed the experiments might fail unexpectedly later.")

from hyperopt import *

alg_fn = {'rs': random_search, 'tpe': tpe}
rng = np.random.default_rng()

def read_h5(loc="./results/tpe-good_range-0.2-10-42-perfs.h5"):
    """
    Gets the data from a specific experiment, returns all values in a dict
    Each row in the dict values is an experiment i.e. history.shape = (3, 150)
                                                        -> 3 experiments, 150 function evaluations
    The header has the corresponding experiment setup data in arrays that correspond to the rows of e.g. history
    """
    data = {}
    with h5py.File(loc, "r") as file:

        for key, val in file.items():
            data[key] = np.array(file[key])

        header = dict(file.attrs)

    return header, data


def run_single_experiment(experiment_data):
    tried_configs, performances = alg_fn[experiment_data['algorithm']](**experiment_data)
    return tried_configs, performances

def run_n_tpe_experiments(setups, n=5, name="", force_different_rng=True):
    # we allow different setups to be passed in order to be able to pass different seeds, all else should be the same

    if isinstance(setups, dict):
        setups = np.full(shape=n, fill_value=setups)

        if force_different_rng:
            for i in range(len(setups)):
                setups[i]["seed"] = int(rng.integers(0, 2_000_000_000, size=1, dtype=int))

    elif isinstance(setups, np.ndarray):
        if len(setups) == n:
            pass
        elif len(setups) == 1:
            setups = np.full(shape=n, fill_value=setups).flatten()
        else:
            raise RuntimeError("Setups must be either all given or all the same.")
    else:
        raise RuntimeError("Setups must be dict or np.array of dicts.")

    try:
        from pathos.pools import ProcessPool
        from pathos.helpers import cpu_count
        n_cores = int(np.floor(cpu_count() * 0.8))
        pool = ProcessPool(nodes=n_cores)

        results = pool.map(run_single_experiment, setups)

        # pool.close()
        # pool.join()

        results = np.array(results, dtype=object)

        histories = results[:, 0].astype(float)
        configss = results[:, 1].astype(dict)

    except ModuleNotFoundError:
        raise NotImplementedError("You can re-run the experiments individually or install pathos.\n"
                                  "Since the results are replicable and this is beyond the requirements of the \n"
                                  "assignment we do not feel the need to implement this manually.")

    file_name = save(experiment_data=setups, histories=histories, configss=configss, name=name)

    try:
        return file_name
    except BaseException:
        return

def prep_dict4h5py(configs):
    shape = configs.shape

    needed_keys = list(set(val for dic in configs.flatten() for val in dic.keys()))

    _configs = dict(zip(needed_keys, np.full(shape=(len(needed_keys), *shape), fill_value=np.nan)))


    for i, run in enumerate(configs):
        for j, entry in enumerate(run):
            for key, value in entry.items():
                try:
                    _configs[key][i, j] = value
                except ValueError:
                    _configs[key] = _configs[key].astype(type(value))
                    _configs[key][i, j] = value

    # for key, value in _configs.items():
    #     print(key,value.shape, value)

    return _configs


def save(experiment_data, histories, configss, dir="./results", name=""):
    """Saves rewards list to directory"""

    Path(dir).mkdir(parents=True, exist_ok=True)

    # even if the name is just one value (e.g.) gamma = 0.2,
    # internally there might be differences which get written into the header.
    # See the read_h5 function for an explanation

    file_name = Path(dir) / f"{name}--"\
                            f"{experiment_data[0]['algorithm']}-"\
                            f"{experiment_data[0]['problem']}-"\
                            f"{experiment_data[0]['gamma']}-"\
                            f"{experiment_data[0]['random_warmup']}.h5"

    try:
        f = h5py.File(file_name, 'w')


        f.create_dataset("history", data=histories)

        configss = prep_dict4h5py(configss)
        for key, value in configss.items():
            try:
                f.create_dataset(key, data=value, dtype=value.dtype)
            except TypeError as e:
                if value.dtype == "<U32": # I hate strings I hate strings I hate strings
                    f.create_dataset(key, data=value.astype(np.bytes_))
                else:
                    raise e

        ### save simulation data to h5 file
        needed_keys, values = experiment_data[0].keys(), experiment_data[0].values()

        meta_dict = dict(zip(needed_keys, np.full(shape=(len(needed_keys), len(experiment_data)), fill_value=np.nan)))

        for i, entry in enumerate(experiment_data):
            for key, value in entry.items():
                try:
                    meta_dict[key][i] = value
                except ValueError:
                    meta_dict[key] = meta_dict[key].astype(type(value))
                    meta_dict[key][i] = value

        # Store metadata in hdf5 file
        for k in meta_dict.keys():
            f.attrs[k] = str(meta_dict[k])
        f.close()
    except RuntimeError:
        print(f"!! File could not be saved !!")
        print(experiment_data)
        raise RuntimeError

    return file_name

def run_boring_experiments():
    methods = ["rs", "tpe"]
    problems = ["interactive", 'good_range', "bad_range"]

    from tqdm import tqdm

    for method in tqdm(methods, desc="method"):
        for problem in tqdm(problems, desc="problem"):
            setup = {
                "problem": problem,
                "algorithm": method,
                "gamma": 0.1,
                "random_warmup": 10,
                "seed": 42,
                "verbose": True
            }

            __ = run_single_experiment(setup)

    return

def run_n_boring_experiments(n=5):
    methods = ["tpe", "rs"]
    problems = ["interactive", 'good_range', "bad_range"]

    for method in methods:
        for problem in problems:
            setup = {
                "problem": problem,
                "algorithm": method,
                "gamma": 0.1,
                "random_warmup": 10,
                "seed": 42,
                "verbose": False
            }

            run_n_tpe_experiments(setup, n=n, name="new_survey", force_different_rng=True)

    return

if __name__ == '__main__':
    # this doesnt produce output on its own :)
    setup = {
        "problem": 'good_range',
        "algorithm": 'tpe',
        "gamma": 0.1,
        "random_warmup": 10,
        "seed": 42,  # !!!!! if seed = int all experiments will return the same values, for random samples use instead:
        # "seed": rng.integers(0, 2_000_000_000, size=1, dtype=int),
        # no idea why the other file uses all the same seed, I guess for grading?
        "verbose": False
    }

    # setup = np.array([
    #     {
    #         "problem": 'good_range',
    #         "algorithm": 'tpe',
    #         "gamma": 0.5,  # <- changes !
    #         "random_warmup": 10,
    #         "seed": 42,  # if seed = int all experiments will return the same values, for random samples use:
    #         # rng.integers(0, 2_000_000_000, size=1, dtype=int)
    #         # no idea why the other file uses all the same seed, I guess for grading?
    #         "verbose": False
    #     },
    #     {
    #         "problem": 'good_range',
    #         "algorithm": 'tpe',
    #         "gamma": 0.1, # <- changes !
    #         "random_warmup": 10,
    #         "seed": 42,  # if seed = int all experiments will return the same values, for random samples use:
    #         # rng.integers(0, 2_000_000_000, size=1, dtype=int)
    #         # no idea why the other file uses all the same seed, I guess for grading?
    #         "verbose": False
    #     },
    #     {
    #         "problem": 'good_range',
    #         "algorithm": 'tpe',
    #         "gamma": 0.01, # <- changes !
    #         "random_warmup": 10,
    #         "seed": 42,  # if seed = int all experiments will return the same values, for random samples use:
    #         # rng.integers(0, 2_000_000_000, size=1, dtype=int)
    #         # no idea why the other file uses all the same seed, I guess for grading?
    #         "verbose": False
    #     }
    # ])

    # run_n_tpe_experiments(setups=setup, n=3)
    #
    # read_h5()

    run_n_boring_experiments(n=50)