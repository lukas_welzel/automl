from copy import deepcopy
from turtle import forward
from pyparsing import Opt
import torch
import torch.nn as nn
import higher
from networks import Conv4

class MAML(nn.Module):

    def __init__(self, num_ways, input_size, T=3, second_order=False, inner_lr=0.4, **kwargs):
        super().__init__()
        self.num_ways = num_ways
        self.input_size = input_size
        self.num_updates = T
        self.second_order = second_order
        self.inner_loss = nn.CrossEntropyLoss()
        self.inner_lr = inner_lr
        self.network = Conv4(self.num_ways, img_size=int(input_size**0.5)) 

    def fill_generator(self,iterable):
        for i in iterable:
            yield i

    # controller input = image + label_previous
    def apply(self, x_supp, y_supp, x_query, y_query, training=True):
        """
        Pefrosmt the inner-level learning procedure of MAML: adapt to the given task 
        using the support set. It returns the predictions on the query set, as well as the loss
        on the query set (cross-entropy).
        You may want to set the gradients manually for the base-learner parameters 

        :param x_supp (torch.Tensor): the support input iamges of shape (num_support_examples, num channels, img width, img height)
        :param y_supp (torch.Tensor): the support ground-truth labels
        :param x_query (torch.Tensor): the query inputs images of shape (num_query_inputs, num channels, img width, img height)
        :param y_query (torch.Tensor): the query ground-truth labels

        :returns:
          - query_preds (torch.Tensor): the predictions of the query inputs
          - query_loss (torch.Tensor): the cross-entropy loss on the query inputs
        """
        # TODO: implement this function

        # Note: to make predictions and to allow for second-order gradients to flow if we want,
        # we use a custom forward function for our network. You can make predictions using
        # preds = self.network(input_data, weights=<the weights you want to use>)
        fast_weights = [param.clone() for param in self.parameters()]
        #Calculate gradients and take step
        for i in range(self.num_updates):
            #fast_weights = [param.clone() for param in fast_weights]
            output = self.network.forward(x_supp, weights = fast_weights) 
            loss = self.inner_loss(output, y_supp)
            if(self.second_order==False):
                grads=torch.autograd.grad(loss, fast_weights, create_graph=False)
            else:
                grads=torch.autograd.grad(loss, fast_weights, create_graph=True)
            grads=list(grads)
            fast_weights = [fast_weights[i]-(self.inner_lr * grads[i]) for i in range(len(fast_weights))]

        preds = self.network(x_query, weights = fast_weights)
        output = self.network.forward(x_query, weights = fast_weights) 
        self.inner_loss.requires_grad = True     
        query_loss = self.inner_loss(output,y_query)
        if training:
            if(self.second_order):
                query_loss.backward(retain_graph = True) # do not remove this if statement, otherwise it won't tain
            else:
                query_loss.backward()
  
        return preds, query_loss