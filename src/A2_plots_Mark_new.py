import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import h5py
from scipy.signal import savgol_filter

labels = [
    "Pnet",
    "1st MAML",
    "2nd MAML"
]

colors = [
    "blue",
    "orange",
    "green",
]

colors2 = [
    "cyan",
    "red",
    "lightgreen",
]

ls = [
    "solid",
    "dashed",
    "dotted"
]

hatch = [
    "/",
    "\\",
    "."
]

def main_task1_plot():
    fig, axes = plt.subplots(1, 2, constrained_layout=True,
                             num="Task1",
                             figsize=(16, 6))

    axes = np.array(axes).flatten()
    yaxes = [
        "Accuracy",
        "Loss / Initial Loss"
    ]

    data_dirs = [
        "results/pnet_infeatures_default.h5",
        "results/Archive/results/1st_maml_default_full.h5",
        "results/Archive/full_results (1)/2nd_maml_default_full.h5"
    ]
    for i in range(len(data_dirs)):
        data_dirs[i] = Path(data_dirs[i])
    

    for j, (ax, method) in enumerate(zip(axes, [plot_accuracy, plot_losses])):
        plt.sca(ax)
        ax.set_ylabel(yaxes[j])
        ax.set_xlabel("Epochs")

        for i, (label, data_dir) in enumerate(zip(labels, data_dirs)):
            header, data = read_h5(data_dir)
            # for key, value in data.items():
            #     print(key, value.shape)
            method(data, label,color=colors[i],color2=colors2[i], ls=ls[i], hatch=hatch[i])

    plt.show()


def read_h5(loc="NULL.h5"):
    """
    Gets the data from a specific experiment, returns all values in a dict
    Each row in the dict values is an experiment i.e. history.shape = (3, 150)
                                                        -> 3 experiments, 150 function evaluations
    The header has the corresponding experiment setup data in arrays that correspond to the rows of e.g. history
    """
    data = {}
    with h5py.File(loc, "r") as file:

        for key, val in file.items():
            data[key] = np.array(file[key])
        header = dict(file.attrs)

    return header, data

def read_result(dir):
    pass

def plot_losses(data, label, color=None, color2=None, hatch=None, ls=None, plot_smooth=False, plot_full=True,
                window=10):
    fig = plt.gcf()
    ax = fig.gca()

    val_losses = data["val_losses"]

    mean_val_losses_1 = np.nanmean(val_losses, axis=(2))
    mean_val_losses = np.nanmean(mean_val_losses_1, axis=(0))

    std_val_losses = np.nanstd(mean_val_losses_1, axis=(0)) #/ np.mean(mean_val_losses[:5])
    x = np.arange(len(mean_val_losses))
    scale = 1.0

    if plot_full:
        ax.plot(x, mean_val_losses,
                label=label+r" $\hat{L}_{val}$",
                color=color,
                ls=ls)

        ax.fill_between(x,
                        mean_val_losses + scale * std_val_losses,
                        mean_val_losses - scale * std_val_losses,
                        label=label+r" $L_{val}$, 1$\sigma$CI",
                        color=color,
                        hatch=hatch,
                        alpha=0.3)
    scale = 0.1

    
        #ax.plot(x, mean_test_losses,
        #        label=label + r" $\hat{L}_{train}$",
        #        color=color2,
        #        ls=ls)

        #ax.fill_between(x,
        #                mean_test_losses + scale * std_test_losses,
        #                mean_test_losses - scale * std_test_losses,
        #                label=label + r" $L_{train}$, 1$\sigma$CI",
        #                color=color2,
        #                hatch=hatch,
        #                alpha=0.3)

    if plot_smooth:
        smooth_mean_losses = np.clip(savgol_filter(mean_losses, window, 1), a_min=0., a_max=None)
        smooth_mean_losses *= 1 / np.mean(smooth_mean_losses[:5])
        #smooth_mean_test_losses = np.clip(savgol_filter(mean_test_losses, window, 1), a_min=0., a_max=None)
        #smooth_mean_test_losses *= 1 / np.mean(smooth_mean_test_losses[:5])

        smooth_std_losses = savgol_filter(std_losses, window, 1) / np.mean(smooth_mean_losses[:5])
    #smooth_mean_losses = mean_losses
    #smooth_std_losses= std_losses
        ax.plot(x, smooth_mean_losses,
                label=label + r" $\hat{L}_{train}$",
                color=color,
                ls=ls)
    #ax.plot(x_loss, smooth_mean_losses,
    #        label=label + r" $\hat{L}_{val}$",
    #        color=color2,
    #        ls=ls)

        #smooth_std_test_losses = savgol_filter(std_test_losses, window, 1) / np.mean(smooth_mean_test_losses[:5])
   # ax.fill_between(x,
   #                 np.clip(smooth_mean_losses + scale * smooth_std_losses, a_min=0., a_max=None),
   #                 np.clip(smooth_mean_losses - scale * smooth_std_losses, a_min=0., a_max=None),
   #                 label=label + r" $L_{train}$, 1$\sigma$CI",
   #                 color=color,
   #                 hatch=hatch,
   #                 alpha=0.3)
    #ax.fill_between(x_loss,
    #                np.clip(smooth_mean_losses + scale * smooth_std_losses, a_min=0., a_max=None),
    #                np.clip(smooth_mean_losses - scale * smooth_std_losses, a_min=0., a_max=None),
    #                label=label + r" $L_{val}$, 1$\sigma$CI",
    #                color=color2,
    #                hatch=hatch,
    #                alpha=0.3)

        #ax.plot(x, smooth_mean_test_losses,
        #        label=label + r" $\hat{L}_{train}$",
        #        color=color2,
        #        ls=ls)

        #ax.fill_between(x,
        #                np.clip(smooth_mean_test_losses + scale * smooth_std_test_losses, a_min=0, a_max=None),
        #                np.clip(smooth_mean_test_losses - scale * smooth_std_test_losses, a_min=0, a_max=None),
        #                label=label + r" $L_{train}$, 1$\sigma$CI",
        #                color=color2,
        #                hatch=hatch,
        #                alpha=0.3)

    ax.legend()
    #ax.set_yscale("symlog")
    ax.set_ylim(0.0, 1.0)

    return

def plot_accuracy(data, label, color=None, hatch=None, ls=None, plot_smooth=True, plot_full=False,
                  window=2, **kwargs):
    ax = plt.gca()

    full_alpha = 0.9
    if plot_smooth:
        full_alpha = 0.1

    accuracies = data["val_accuracies"]

    mean_accuracies_1 = np.nanmean(accuracies, axis=(2))
    mean_accuracies = np.nanmean(mean_accuracies_1, axis=(0))

    std_accuracies = np.nanstd(mean_accuracies_1, axis=(0))
    #mean_accuracies = np.mean(accuracies, axis=(0))
    #std_accuracies = np.std(accuracies, axis=(0))
    x = np.arange(len(mean_accuracies))
    if plot_full:
        ax.plot(x, mean_accuracies,
                label=label+r" $\hat{Acc}$",
                color=color,
                ls=ls)

        ax.fill_between(x,
                        mean_accuracies + 1 * std_accuracies,
                        mean_accuracies - 1 * std_accuracies,
                        label=label+r" $\hat{Acc}$, 1$\sigma$CI",
                        color=color,
                        hatch=hatch,
                        alpha=full_alpha)
    if plot_smooth:
        smooth_mean_acc = np.clip(savgol_filter(mean_accuracies, window, 1), a_min=0, a_max=1.)
        smooth_std_acc = savgol_filter(std_accuracies, window, 1)
        ax.plot(x, smooth_mean_acc,
                label=label + r" $\hat{Acc}_{smooth}$",
                color=color,
                ls=ls,
                alpha=1.)

        ax.fill_between(x,
                        np.clip(smooth_mean_acc + 1 * smooth_std_acc, a_min=0, a_max=1.),
                        np.clip(smooth_mean_acc - 1 * smooth_std_acc, a_min=0, a_max=1.),
                        label=label + r" $\hat{Acc}$, 1$\sigma$CI  (smooth)",
                        color=color,
                        hatch=hatch,
                        alpha=0.5)

    ax.legend()
    ax.set_ylim(0, None)

    return



if __name__ == '__main__':
    main_task1_plot()