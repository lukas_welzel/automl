import argparse

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import os

from pnet import PrototypicalNetwork
from maml import MAML
from dataloaders import train_val_test_loaders, extract_task
from pathlib import Path
import h5py
from tqdm import tqdm

class Args(object):
    """
    Class for interfacing with the beautifully written main.py.
    Seriously, why not make a functional interface so that we can just set up many experiments?
    """
    def __init__(self,
                 algorithm="pnet",
                 num_ways=5,
                 num_support_shots=5,
                 num_query_shots=15,
                 meta_batch_size=1,
                 val_interval=500,
                 num_eval_tasks=1000,
                 num_train_episodes=40000,
                 lr=1e-3,
                 inner_lr=0.4,
                 second_order=False,
                 dataset="omniglot",
                 T=1,
                 img_size=28,
                 rgb="store_true",
                 dev=0,
                 seed=0, **kwargs):
        super(Args, self).__init__()

        self.algorithm = algorithm
        self.num_ways = num_ways
        self.num_support_shots = num_support_shots
        self.num_query_shots = num_query_shots
        self.meta_batch_size = meta_batch_size
        self.val_interval = val_interval
        self.num_eval_tasks = num_eval_tasks
        self.num_train_episodes = num_train_episodes
        self.lr = lr
        self.inner_lr = inner_lr
        self.second_order = second_order
        self.dataset = dataset
        self.T = T
        self.img_size = img_size
        self.rgb = rgb
        self.dev = dev
        if seed == 0:  # seriously...
            self.seed = np.random.randint(1, 100000)
        else:
            self.seed = seed

def run_many(n=3, algo="pnet", second_order=False, meta_batch_size=1, num_ways=5, experiment_name="placeholder", **kwargs):
    ##############################################################################################################
    # Create result storage location
    RDIR = "./results/"

    ##############################################################################################################

    args = Args(algorithm=algo, second_order=second_order, meta_batch_size=meta_batch_size, num_ways=num_ways,
                **kwargs)

    print(vars(args))

    # for key, value in vars(args).items():
    #     print(F"{key}, {value}")

    results = {
        "losses": np.full((n, args.num_train_episodes), fill_value=np.nan),
        "accuracies": np.full((n, args.num_train_episodes), fill_value=np.nan),
        "val_losses": np.full((n, int(args.num_train_episodes / args.val_interval), args.num_eval_tasks), fill_value=np.nan),
        "val_accuracies": np.full((n, int(args.num_train_episodes / args.val_interval), args.num_eval_tasks), fill_value=np.nan),
        "test_losses": np.full((n, args.num_eval_tasks), fill_value=np.nan),
        "test_accuracies": np.full((n, args.num_eval_tasks), fill_value=np.nan)
    }

    seeds = []


    for i in tqdm(range(n)):
        args = Args(algorithm=algo, second_order=second_order, meta_batch_size=meta_batch_size, num_ways=num_ways,
                    **kwargs)
        seeds.append(args.seed)

        print("Running: ", vars(args))

        losses, accuracies, val_losses, val_accuracies, test_losses, test_accuracies = main_experiment(args)

        # for i, el in enumerate([losses, accuracies, val_losses, val_accuracies, test_losses, test_accuracies]):
        #     print(np.array(el).shape)
        #
        # for key, value in results.items():
        #     print(f"{key}, {value.shape}")

        results["losses"][i] = losses
        results["accuracies"][i] = accuracies
        results["val_losses"][i] = val_losses
        results["val_accuracies"][i] = val_accuracies
        results["test_losses"][i] = test_losses
        results["test_accuracies"][i] = test_accuracies

        # plt.plot(results["losses"][i])
        # plt.show()

    # not needed anymore
    # for key, value in results.items():
    #     results[key] = value[:, :np.min(np.sum(~ np.isnan(value), axis=1), axis=0)]  # hacky

    meta_dict = vars(args)
    meta_dict["seed"] = ", ".join(str(seeds))

    save(results, meta_dict, dir=RDIR, name=experiment_name)

def put_on_device(dev, tensors):
    """
    Puts the given tensors on the given device.

    :param dev (str): the device identifier ("cpu"/"cuda:<GPU-ID>")
    :param tensors [list]: a list of tensors that we want to put on the device

    :return (list): list of tensors placed on the device
    """
    for i in range(len(tensors)):
        if not tensors[i] is None:
            tensors[i] = tensors[i].to(dev)
    return tensors

def main_experiment(args):
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    ##############################################################################################################

    def validate_performance(model, val_loader):
        """
        Function that evaluates the performance of the model on tasks from the given dataloader (either val/test)

        :param model (torch.nn.Module): the model we evaluate
        :param val_loader (dataloader): the validation dataloader

        :return:
          - vloss (list): list of validation losses on the evaluation tasks
          - vacc (list): list of validation accuracy scores on the evaluation tasks
        """
        vloss, vacc = [], []
        for vid, batch in enumerate(val_loader, start=1):
            support_inputs, support_targets, query_inputs, query_targets = extract_task(batch)
            support_inputs, support_targets, query_inputs, query_targets = put_on_device(args.dev, [support_inputs,
                                                                                                    support_targets,
                                                                                                    query_inputs,
                                                                                                    query_targets])
            # compute model predictions on the query set, conditioned on the support set
            preds, loss = model.apply(support_inputs, support_targets, query_inputs, query_targets)
            accuracy = (torch.sum(torch.argmax(preds, dim=1) == query_targets).item() / query_targets.size(0))
            # log scores
            vloss.append(loss.item())
            vacc.append(accuracy)

            if vid == args.num_eval_tasks:
                break

        return vloss, vacc

    # Create the data loaders
    dataloader_config = {
        "num_ways": args.num_ways,
        "num_support_shots": args.num_support_shots,
        "num_query_shots": args.num_query_shots,
        "ds": args.dataset,
        "img_size": args.img_size,
        "rgb": args.rgb,
    }
    train_loader, val_loader, test_loader = train_val_test_loaders(**dataloader_config)

    if args.dev is None:
        args.dev = "cpu"
    else:
        # args.dev = "cuda:0"
        try:
            torch.cuda.set_device(args.dev)
        except:
            print("Could not connect to GPU 0")
            import sys;
            sys.exit()

    # Define the model that we use
    if args.algorithm == "maml":
        constr = MAML
    else:
        constr = PrototypicalNetwork

    model = constr(num_ways=args.num_ways, T=args.T, input_size=args.img_size ** 2, rgb=args.rgb,
                   img_size=args.img_size)  # images are of size 28x28
    model = model.to(args.dev)
    loss_fn = nn.CrossEntropyLoss()
    opt = torch.optim.Adam(model.parameters(), lr=args.lr / args.meta_batch_size)

    losses = []
    accuracies = []
    val_losses = []
    val_accuracies = []
    best_val_acc = -float("inf")
    best_parameters = [p.clone().detach() for p in model.parameters()]
    force_validation = False
    # Main loop
    for bid, batch in enumerate(train_loader, start=1):
        # support_inputs shape: (num_support_examples, num channels, img width, img height)
        # support targets shape: (num_support_labels)
        # support targets shape: (num_support_labels)

        # query_inputs shape: (num_query_inputs, num channels, img width, img height)
        # query_targets shape: (num_query_labels)
        support_inputs, support_targets, query_inputs, query_targets = extract_task(
            batch)  # extract a single task from the loader
        support_inputs, support_targets, query_inputs, query_targets = put_on_device(args.dev,
                                                                                     [support_inputs, support_targets,
                                                                                      query_inputs, query_targets])

        # compute model predictions on the query set, conditioned on the support set
        preds, loss = model.apply(support_inputs, support_targets, query_inputs, query_targets, training=True)

        if bid % args.meta_batch_size == 0:
            # update the parameters of the model using Adam
            opt.step()
            # zero the gradient buffers for next usage
            opt.zero_grad()

        # log the observed loss and accuracy score
        losses.append(loss.item())
        accuracy = (torch.sum(torch.argmax(preds, dim=1) == query_targets).item() / query_targets.size(0))
        accuracies.append(accuracy)

        if (bid / args.meta_batch_size) >= args.num_train_episodes:
            force_validation = True

        # Meta-validation
        if (bid / args.meta_batch_size) % args.val_interval == 0 or force_validation:
            model.eval()
            vloss, vacc = validate_performance(model, val_loader)
            val_losses.append(vloss)
            val_accuracies.append(vacc)

            avg_val_acc = np.mean(vacc)
            print("Mean validation loss:", avg_val_acc, "mean training accuracy:", np.mean(accuracies))
            # if we exceed the incumbent best performance, store a copy of the weights so that we can use them for testing
            if avg_val_acc > best_val_acc:
                best_parameters = [p.clone().detach() for p in model.parameters()]
                best_val_acc = avg_val_acc
            model.train()

        if force_validation:
            break

    # Load the best found parameters from validation
    for current_param, best_param in zip(model.parameters(), best_parameters):
        current_param.data = best_param.data

    model.eval()
    test_losses, test_accuracies = validate_performance(model, test_loader)

    # plt.plot(test_losses)
    # plt.show()

    return losses, accuracies, val_losses, val_accuracies, test_losses, test_accuracies


def save(data, header, dir="./results", name=""):
    Path(dir).mkdir(parents=True, exist_ok=True)

    # even if the name is just one value (e.g.) gamma = 0.2,
    # internally there might be differences which get written into the header.
    # See the read_h5 function for an explanation

    file_name = Path(dir) / f"{name}.h5"

    try:
        f = h5py.File(file_name, 'w')

        for key, value in data.items():
            f.create_dataset(key, data=value)

        # Store metadata in hdf5 file
        for k in header.keys():
            f.attrs[k] = str(header[k])
        f.close()
    except RuntimeError:
        print(f"!! File could not be saved !!")
        print(name)
        raise RuntimeError

if __name__ == '__main__':
    run_many(3, algo="pnet", experiment_name="pnet_infeatures_default_num_ways20",
             val_interval=500,
             num_eval_tasks=1000,
             num_train_episodes=40000,
             num_ways=20)
