import numpy as np
import matplotlib.pyplot as plt
rng = np.random.default_rng()


class CustomDistribution:
    """
    Based on https://stackoverflow.com/questions/29095070/how-to-simulate-from-an-arbitrary-continuous-probability-distribution
    """
    def __init__(self, x, pdf, **kwargs):
        self.x = x
        self.pdf = pdf

    def update(self, pdf):
        self.pdf = pdf

    def sample(self, n, **kwargs):
        cumpdf = np.cumsum(self.pdf)
        cumpdf *= 1 / cumpdf[-1]

        unif_samples = rng.uniform(size=n)

        ind1 = np.searchsorted(cumpdf, unif_samples)

        ind0 = np.where(ind1 == 0, 0, ind1 - 1)
        ind1[ind0 == 0] = 1

        frac1 = (unif_samples - cumpdf[ind0]) / (cumpdf[ind1] - cumpdf[ind0])
        samples = self.x[ind0] * (1 - frac1) + self.x[ind1] * frac1
        return samples


def test_loguniform(distr_resolution=100):
    # reals
    interval = [0.001, 1.]
    low = np.log10(interval[0])
    high = np.log10(interval[1])

    x = np.geomspace(np.min(interval),
                          np.max(interval),
                          distr_resolution)

    _x = np.linspace(np.log10(np.max(np.min(interval)), dtype=float),
                          np.log10(np.min(np.max(interval)), dtype=float),
                          distr_resolution)

    pdf = np.linspace(0.001, 1, len(_x))**6

    dist = CustomDistribution(_x, pdf)

    samples = np.power(10, dist.sample(10000))

    fig, (ax1, ax2) = plt.subplots(2, 1)

    hist, bins, _ = ax1.hist(samples, bins=100)

    # histogram on log scale.
    # Use non-equal bin sizes, such that they look equal on log scale.
    logbins = np.logspace(np.log10(bins[0]), np.log10(bins[-1]), len(bins))


    ax2.hist(x, bins=logbins, width=0.00005)

    ax2.set_xscale("log")
    plt.show()

    # ints

    interval = [1, 100.]
    low = np.log10(interval[0])
    high = np.log10(interval[1])

    x = np.geomspace(np.min(interval),
                     np.max(interval),
                     distr_resolution)

    _x = np.linspace(np.log10(np.max(np.min(interval)), dtype=float),
                     np.log10(np.min(np.max(interval)), dtype=float),
                     distr_resolution)

    pdf = np.linspace(0.001, 1, len(_x))**6

    dist = CustomDistribution(_x, pdf)

    samples = np.power(10, dist.sample(10000)).astype(int)

    fig, (ax1, ax2) = plt.subplots(2, 1)

    hist, bins, _ = ax1.hist(samples, bins=100, width=0.5)

    # histogram on log scale.
    # Use non-equal bin sizes, such that they look equal on log scale.
    # logbins = np.logspace(np.log10(bins[0]), np.log10(bins[-1]), len(bins))
    #
    # ax2.hist(x, bins=logbins)
    #
    # ax2.set_xscale("log")
    plt.show()






if __name__ == '__main__':
    test_loguniform()


